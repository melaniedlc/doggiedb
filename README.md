# DoggieDB

## Website Url
https://doggiedb.me

## Presentation Video
https://youtu.be/kLSc5EA57Ek

## Team Members
| Name | EID | GitLab ID |
|---|---|---|
| Sriram Alagappan | sva359 | @sriramalagappan |
| Melanie De La Cruz  | med3358 | @melaniedlc |
| Prateeka Kodali | pk8793 | @prateekakodali |
| Jack Raney | jrr487 | @RaneyJ |
| Patricio Rodriguez | pir229 | @patricior |

### Project Leader: 
|Phase | Name |
|---|---|
| I | Melanie De La Cruz |
| II | Jack Raney |
| III | Patricio Rodriguez |
| IV | Prateeka Kodali |

## Completion Times
### Phase I:
| Name | Estimated | Actual |
| --- | --- | --- |
| Sriram Alagappan | 14 | 22 |
| Melanie De La Cruz | 15 | 24 |
| Prateeka Kodali | 15 | 23 |
| Jack Raney | 14 | 24 |
| Patricio Rodriguez | 12 | 22 |

### Phase II:
| Name | Estimated | Actual |
| --- | --- | --- |
| Sriram Alagappan | 18 | 42 |
| Melanie De La Cruz | 20 | 45 |
| Prateeka Kodali | 28 | 45 |
| Jack Raney | 20 | 48 |
| Patricio Rodriguez | 18 | 42 |

### Phase III:
| Name | Estimated | Actual |
| --- | --- | --- |
| Sriram Alagappan | 24 | 27 |
| Melanie De La Cruz | 26 | 28 |
| Prateeka Kodali | 30 | 28 |
| Jack Raney | 28 | 29 |
| Patricio Rodriguez | 25 | 27 |

### Phase IV:
| Name | Estimated | Actual |
| --- | --- | --- |
| Sriram Alagappan | 10 | 11 |
| Melanie De La Cruz | 12 | 12 |
| Prateeka Kodali | 15 | 11 |
| Jack Raney | 14 | 12 |
| Patricio Rodriguez | 12 | 10 |

## Postman
https://documenter.getpostman.com/view/14754594/TzJrDf1E

## Pipelines
https://gitlab.com/melaniedlc/doggiedb/-/pipelines

## Git SHA
3cbd3835a2a70df2f7526454defeaf34fa248cff

## Comments
We spent over 75% of the time working as a group during this phase.
