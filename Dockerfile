FROM nikolaik/python-nodejs

COPY . /app
RUN ls
WORKDIR /app

RUN pip install -r ./backend/requirements.txt
RUN pip3 install psycopg2

RUN cd ./frontend && npm install && npm run build

EXPOSE 5000

COPY . /app

ENTRYPOINT [ "python" ]
CMD [ "./backend/main.py" ]
