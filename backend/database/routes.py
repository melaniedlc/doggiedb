import json
from flask import Flask, request, abort

# from flask_cors import CORS
from gitlab_api import get_statistics
from database import db
from .models import City, Breed, Shelter, Dog
from flask import current_app as app
from sqlalchemy.orm import joinedload
from sqlalchemy import asc, desc, func

# from test import dog_to_db, shelter_to_db, breed_to_db
# from get_shelter_cities import main
from update_relationals import build_relations, clear_empty_cities
from geocode_cities import get_coords
#from dev_data import generate_breed_pie, generate_shelter_count, generate_all_shelter_count, generate_mississippi_shelter_count, generate_map_data

from urllib.parse import parse_qs
import re

import math
import numpy as np


def filterAndSortModelFromQueryString(
    model, query, operationHandler=lambda query, operation, key, value: None
):
    queryString = parse_qs(request.query_string.decode())

    # print(queryString)

    for key in queryString:
        val = queryString[key][0]
        val2 = ""
        try:
            val2 = queryString[key][1]
        except:
            pass
        # print(val2)

        match = re.search("(filter|sort)\[(.+)\]", key)
        if match:
            operation = match.group(1)

            handled = operationHandler(query, operation, match.group(2), val)

            # see if the operationHandler handled this case
            if handled is not None:
                query = handled
            # see if we can handle this case automatically
            else:
                column = getattr(model, match.group(2), None)
                if column is not None:
                    if operation == "filter":
                        if val2 == "":
                            query = query.filter(column.contains(val))
                        else:
                            #IF WE HAVE 2 VALUES, WE WILL COMPUTE OVER A RANGE

                            #FOR STRINGS: COMPUTE RANGE OF RESPONSES STARTING WITH VAL1 THROUGH THOSE STARTING WITH VAL2
                            #FOR NUM: COMPUTE RANGE OF RESPONSES BETWEEN VAL1 AND VAL2
                            #Might have to differentiate between Int and Float
                            if str(column.type) == "VARCHAR(255)":
                                # print("STRING")
                                val1_parse = str(val)[:1]
                                val2_parse = str(val2)[:1]
                                min_str = min(val, val2)
                                max_str = max(val, val2)
                                if max_str == 'z':
                                    max_str = 'zz'
                                elif max_str == 'Z':
                                    max_str = 'ZZ'
                                else:
                                    temp = ord(max_str)
                                    temp += 1
                                    max_str = chr(temp)
                                # print(min_str, max_str)
                                # print(column)
                                query = query.filter(column.between(min_str, max_str))
                                # print(query.all())
                            else:
                                # print("NUM")
                                val1_parse = float(val)
                                val2_parse = float(val2)
                                min_float = min(val1_parse, val2_parse)
                                max_float = max(val1_parse, val2_parse)
                                # print(min_float, max_float)
                                # print(column)
                                #want column val to be between min and max float
                                query = query.filter(column.between(min_float, max_float))
                                # print(query.all())

                    elif operation == "sort":
                        if val == "asc":
                            query = query.order_by(column.asc())
                        elif val == "desc":
                            query = query.order_by(column.desc())

    return query

def paginateQuery(query):
    return query.paginate(page=None, per_page=None, error_out=False, max_per_page=50)


def outputPage(page, itemMapper=lambda x: x.as_dict()):
    return {
        "pagination": {
            "page": page.page,
            "per_page": page.per_page,
            "prev_page": (page.prev_num if page.has_prev else None),
            "next_page": (page.next_num if page.has_next else None),
            "total_pages": page.pages,
            "total_items": page.total,
        },
        "items": [itemMapper(i) for i in page.items],
    }


@app.route("/test")
def testdb():
    #generate_shelter_count()
    #generate_all_shelter_count()
    #generate_mississippi_shelter_count()
    #generate_map_data()
    return ""


# NOTE: This route is needed for the default EB health check route
@app.route("/")
def home():
    return "API v 4.0.0"


@app.route("/api")
def api():
    return "This is DoggieDB API v4.0.0"

@app.route("/api/version")
def version():
    return "This is DoggieDB API v4.0.0"


@app.route("/api/get_stats")
def get_stats():
    return get_statistics()


# Return data on all breeds
@app.route("/api/breeds")
def get_breeds():
    return outputPage(paginateQuery(filterAndSortModelFromQueryString(Breed, Breed.query)))


@app.route("/api/breeds/<breed_id>")
def get_breed(breed_id):
    breed = Breed.query.filter_by(id=breed_id).first()
    if breed is None:
        abort(404)

    r = breed.as_dict()
    r["cities"] = [x.as_dict() for x in breed.cities]
    r["shelters"] = [x.as_dict() for x in breed.shelters]
    # this can potentially dump thousands of records
    r["dogs"] = [x.as_dict() for x in breed.dogs]
    return r


@app.route("/api/shelters")
def get_shelters():
    def mapper(shelter):
        d = shelter.as_dict()
        d["available_breed_names"] = [breed.name for breed in shelter.breeds]
        return d

    def operationHandler(query, operation, key, value):
        if key == 'available_breed_names':
            if operation == 'filter':
                return query.filter(Shelter.breeds.any(Breed.name.contains(value)))
            if operation == 'sort':
                if value == "asc":
                    return query # INTERCEPT SORT ASC HERE
                    # return query.order_by(Shelter.breed_count.asc())
                elif value == "desc":
                    return query # INTERCEPT SORT DESC HERE
                    # return query.order_by(Shelter.breed_count.desc())

        return None

    return outputPage(paginateQuery(filterAndSortModelFromQueryString(Shelter, Shelter.query.options(joinedload(Shelter.breeds)), operationHandler)), mapper)


@app.route("/api/shelters/<shelter_id>")
def get_shelter(shelter_id):

    shelter = Shelter.query.filter_by(id=shelter_id).first()
    if shelter is None:
        abort(404)

    r = shelter.as_dict()
    if shelter.city is not None:
        r["city"] = shelter.city.as_dict()
    r["breeds"] = [x.as_dict() for x in shelter.breeds]
    # this can potentially dump thousands of records
    r["dogs"] = [x.as_dict() for x in shelter.dogs]
    return r


@app.route("/api/cities")
def get_cities():
    return outputPage(paginateQuery(filterAndSortModelFromQueryString(City, City.query)))


@app.route("/api/cities/<city_id>")
def get_city(city_id):
    city = City.query.filter_by(id=city_id).first()
    if city is None:
        abort(404)

    r = city.as_dict()
    r["shelters"] = [x.as_dict() for x in city.shelters]
    r["breeds"] = [x.as_dict() for x in city.breeds]
    # this can potentially dump thousands of records
    r["dogs"] = [x.as_dict() for x in city.dogs]
    return r


# NEED TO UNPICKLE SOME FIELDS (ex: tags)
@app.route("/api/dogs")
def get_dogs():
    return outputPage(paginateQuery(filterAndSortModelFromQueryString(Dog, Dog.query)))


@app.route("/api/dogs/<dog_id>")
def get_dog(dog_id):
    dog = Dog.query.filter_by(id=dog_id).first()
    if dog is None:
        abort(404)

    r = dog.as_dict()
    if dog.shelter is not None:
        r["shelter"] = dog.shelter.as_dict()
    if dog.breed is not None:
        r["breed"] = dog.breed.as_dict()
    if dog.city is not None:
        r["city"] = dog.city.as_dict()
    return r

@app.route("/api/nearby")
def get_nearby():
    #GOAL: Given a point and a radius, return all cities within that radius
    # 1) Get all cities from DB (slow)
    # 2) for each city, run distance formula and keep it if < radius (O(n))

    #Alternatively
    # 1) Get bounded cities (faster)

    queryString = parse_qs(request.query_string.decode())
    keys = queryString.keys()

    rad = 0.0
    lat = 0.0
    lon = 0.0

    if "rad" in keys:
        rad = rad = float(queryString['rad'][0])
    else:
        #400 Bad Request
        abort(400)

    if "city" in keys and "state" in keys:
        city = queryString['city'][0]
        state = queryString['state'][0]
        obj = City.query.filter_by(name=city, state=state).first()
        if obj is None:
            #This city state combo did not exist in our DB
            abort(404)

        lat = obj.latitude
        lon = obj.longitude

        if lat is None or lon is None:
            #this should never happen anymore
            abort(404)

    elif "lat" in keys and "lon" in keys:
        lat = float(queryString['lat'][0])
        lon = float(queryString['lon'][0])
    else:
        #400 Bad Request
        abort(400)

    print(lat, lon, rad)
    lat_ratio = rad/60 #For a change by 1 in latitude there is a change of 60 miles
    lon_ratio = rad/60 #For a change by 1 in long three is a change of at most 60 miles
    lower_lat = lat - lat_ratio
    upper_lat = lat + lat_ratio
    lower_lon = lon - lon_ratio
    upper_lon = lon + lon_ratio

    print(lower_lat, upper_lat)
    print(lower_lon, upper_lon)
    cities = City.query.filter(City.latitude > lower_lat).filter(City.latitude < upper_lat).filter(City.longitude > lower_lon).filter(City.longitude < upper_lon).all()
    in_rad = []
    for city in cities:
        dist = get_distance(lat, city.latitude, lon, city.longitude)
        print(city.name, dist)
        if dist <= rad:
            r = city.as_dict()
            # r["shelters"] = [x.as_dict() for x in city.shelters]
            # r["breeds"] = [x.as_dict() for x in city.breeds]
            # this can potentially dump thousands of records
            # r["dogs"] = [x.as_dict() for x in city.dogs]
            in_rad.append(r)

    response = {}
    response["items"] = in_rad
    return response

#Helper func for get_nearby
def get_distance(lat1, lat2, lon1, lon2):
    earth_rad = 3958.8 #in miles

    x = np.deg2rad(lon2-lon1) * math.cos(np.deg2rad(lat1+lat2)/2)
    y = np.deg2rad(lat1-lat2)
    distance = math.sqrt(x*x + y*y) * earth_rad
    return distance


if __name__ == "__main__":
    app.run(debug=True, port=8080)