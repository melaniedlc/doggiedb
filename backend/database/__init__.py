from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

db = SQLAlchemy()


def init_app():
    """Construct the core application."""
    app = Flask(__name__)
    CORS(app)
    app.config.from_object('config')
    db.init_app(app)

    with app.app_context():
        from . import routes  # Import routes
        #db.create_all()  # Create sql tables for our data models

    return app