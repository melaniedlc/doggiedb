from database import db
import json

breeds_cities = db.Table(
    "breeds_cities",
    db.Column("breed_id", db.Integer, db.ForeignKey("breed.id"), primary_key=True),
    db.Column("city_id", db.Integer, db.ForeignKey("city.id"), primary_key=True),
)

breeds_shelters = db.Table(
    "breeds_shelters",
    db.Column("breed_id", db.Integer, db.ForeignKey("breed.id"), primary_key=True),
    db.Column("shelter_id", db.Integer, db.ForeignKey("shelter.id"), primary_key=True),
)


class City(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    state = db.Column(db.String(255), nullable=True)
    
    #temp values from the last year
    avg_temp = db.Column(db.String(255), nullable=True)
    min_temp = db.Column(db.String(255), nullable=True)
    max_temp = db.Column(db.String(255), nullable=True)

    #pop & pop density only filled for cities with >50,000 pop
    pop = db.Column(db.String(255), nullable=True)
    pop_density = db.Column(db.String(255), nullable=True)
    
    latitude = db.Column(db.String(255), nullable=True)
    longitude = db.Column(db.String(255), nullable=True)

    shelters = db.relationship("Shelter", backref="city")
    dogs = db.relationship("Dog", backref="city")

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return "<City %r>" % self.name


class Breed(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    height = db.Column(db.String(255), nullable=True)
    weight = db.Column(db.Float, nullable=True)
    lifespan = db.Column(db.Float, nullable=True)
    image = db.Column(db.String(255), nullable=True)
    classification = db.Column(db.String(255), nullable=True)
    bred_for = db.Column(db.String(255), nullable=True)
    origin = db.Column(db.String(255), nullable=True)
    temperament = db.Column(db.String(255), nullable=True)
    lat = db.Column(db.Float, nullable=True)
    long = db.Column(db.Float, nullable=True)

    dogs = db.relationship("Dog", backref="breed")

    cities = db.relationship(
        "City",
        secondary=breeds_cities,
        lazy="subquery",
        backref=db.backref("breeds", lazy="subquery"),
    )

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return "<Breed %r>" % self.name

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


class Shelter(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    state = db.Column(db.String(255), nullable=True)
    city_name = db.Column(
        db.String(255), nullable=True
    )
    address = db.Column(db.String(255), nullable=True)
    dog_count = db.Column(db.Integer, nullable=True)
    website = db.Column(db.String(255), nullable=True)
    email = db.Column(db.String(255), nullable=True)
    phone = db.Column(db.String(255), nullable=True)
    pet_finder = db.Column(db.String(255), nullable=True)
    adoption_policy = db.Column(db.String(255), nullable=True)
    photo = db.Column(db.String(255), nullable=True)
    facebook = db.Column(db.String(255), nullable=True)
    twitter = db.Column(db.String(255), nullable=True)
    youtube = db.Column(db.String(255), nullable=True)
    instagram = db.Column(db.String(255), nullable=True)
    pinterest = db.Column(db.String(255), nullable=True)
    animals = db.Column(db.String(255), nullable=True)

    city_id = db.Column(db.Integer(), db.ForeignKey("city.id"))

    breeds = db.relationship(
        "Breed",
        secondary=breeds_shelters,
        lazy="subquery",
        backref=db.backref("shelters", lazy="subquery"),
    )

    dogs = db.relationship("Dog", backref="shelter")

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return "<Shelter %r>" % self.name

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


class Dog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    breed_primary = db.Column(db.String(255), nullable=True)
    breed_secondary = db.Column(db.String(255), nullable=True)
    #state = db.Column(db.String(255), nullable=True)
    #city_name = db.Column(db.String(255), nullable=True)
    #address = db.Column(db.String(255), nullable=True)
    age = db.Column(db.String(255), nullable=True)
    gender = db.Column(db.String(255), nullable=True)
    size = db.Column(db.String(255), nullable=True)
    color_primary = db.Column(db.String(255), nullable=True)
    color_secondary = db.Column(db.String(255), nullable=True)
    email = db.Column(db.String(255), nullable=True)
    phone = db.Column(db.String(255), nullable=True)
    pet_finder = db.Column(db.String(255), nullable=True)
    photo = db.Column(db.String(255), nullable=True)
    photos = db.Column(db.Text(), nullable=True)  # LIST of strings
    videos = db.Column(db.Text(), nullable=True)  # LIST of strings
    tags = db.Column(db.Text(), nullable=True)  # LIST of strings
    spayed_neutered = db.Column(db.Boolean(255), nullable=True)  # BOOL
    house_trained = db.Column(db.Boolean(255), nullable=True)  # BOOL
    special_needs = db.Column(db.Boolean(255), nullable=True)  # BOOL
    shots_current = db.Column(db.Boolean(255), nullable=True)  # BOOL
    good_children = db.Column(db.Boolean(255), nullable=True)  # BOOL
    good_dogs = db.Column(db.Boolean(255), nullable=True)  # BOOL
    good_cats = db.Column(db.Boolean(255), nullable=True)  # BOOL
    #description = db.Column(db.String(4096), nullable=True)
    shelter_name = db.Column(db.String(255), nullable=True)
    shelter_id = db.Column(db.Integer(), db.ForeignKey("shelter.id"))
    city_id = db.Column(db.Integer(), db.ForeignKey("city.id"))
    breed_id = db.Column(db.Integer(), db.ForeignKey("breed.id"))
    pet_finder_id = db.Column(db.String(255), nullable=True)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


    def __repr__(self):
        return "<Breed %r>" % self.name

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
