from database import db
from database.models import City, Breed, Shelter, Dog
import requests
import json
from opencage.geocoder import OpenCageGeocode

scores = {"leisure": 0, "waterway": 0, "road": 0, "highway": 0, "landuse": 0, "railway": 0, "attraction": 0, "neighbourhood": 0, "school": 0, "camp_site": 0, "charging_station": 0.5, "cinema": 0.5, "building": 0.5, "county": 1, "boundary": 1, "municipality": 1.5, "townhall": 2, "village": 2, "city": 3}

def get_coords():
    print("IN GET COORDS")
    key = "ff50c3d1a7e64953a379df0550ce35a0"
    geocoder = OpenCageGeocode(key)
    print("getting shelters")
    cities = City.query.filter_by(latitude=None, longitude=None).all()
    print("STARTING")

    for city in cities:
        print("-----------------")
        query = city.name + "," + city.state
        result = geocoder.geocode(query)
        best_type = ""
        best_index = -1
        index = 0
        for entry in result:
            print(entry["components"]["_type"])
            if best_type == "":
                best_type = entry["components"]["_type"]
                best_index = index
            elif scores[best_type] < scores[entry["components"]["_type"]]:
                best_type = entry["components"]["_type"]
                best_index = index

            index += 1
        print("CHOSE:", best_type)
        lat = result[best_index]["geometry"]["lat"]
        lng = result[best_index]["geometry"]["lng"]
        city.latitude = lat
        city.longitude = lng
        db.session.commit()