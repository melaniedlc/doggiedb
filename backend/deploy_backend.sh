echo "Deploying Backend..."
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 716899554053.dkr.ecr.us-east-1.amazonaws.com/doggiedb-backend
docker build -t doggiedb-backend .
docker tag doggiedb-backend:latest 716899554053.dkr.ecr.us-east-1.amazonaws.com/doggiedb-backend:latest
docker push 716899554053.dkr.ecr.us-east-1.amazonaws.com/doggiedb-backend:latest
cd aws_deploy
eb deploy