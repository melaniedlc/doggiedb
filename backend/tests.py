from unittest import main, TestCase
import requests
import json


class Tests(TestCase):
    def test_breeds(self):
        result = requests.get("https://www.doggiedb.me/api/breeds")
        assert result.status_code == 200
    
    def test_shelters(self):
        result = requests.get("https://www.doggiedb.me/api/shelters")
        assert result.status_code == 200

    def test_cities(self):
        result = requests.get("https://www.doggiedb.me/api/cities")
        assert result.status_code == 200

    def test_dogs(self):
        result = requests.get("https://www.doggiedb.me/api/dogs")
        assert result.status_code == 200

    def test_not_found_breeds(self):
        result = requests.get("https://www.doggiedb.me/api/dogs/id=-1")
        assert result.status_code == 404
    
    def test_not_found_shelters(self):
        result = requests.get("https://www.doggiedb.me/api/dogs/id=-1")
        assert result.status_code == 404

    def test_not_found_cities(self):
        result = requests.get("https://www.doggiedb.me/api/dogs/id=-1")
        assert result.status_code == 404

    def test_not_found_dogs(self):
        result = requests.get("https://www.doggiedb.me/api/dogs/id=-1")
        assert result.status_code == 404

    def test_dog_page(self):
        result = requests.get("https://www.doggiedb.me/api/dogs?page=1")
        r = result.json()
        assert r["pagination"]["per_page"] == len(r["items"])

    def test_city_page(self):
        result = requests.get("https://www.doggiedb.me/api/cities?page=1")
        r = result.json()
        assert r["pagination"]["per_page"] == len(r["items"])

    def test_shelter_page(self):
        result = requests.get("https://www.doggiedb.me/api/shelters?page=1")
        r = result.json()
        assert r["pagination"]["per_page"] == len(r["items"])

    def test_breed_page(self):
        result = requests.get("https://www.doggiedb.me/api/breeds?page=1")
        r = result.json()
        assert r["pagination"]["per_page"] == len(r["items"])

    def test_max_page(self):
        result = requests.get("https://www.doggiedb.me/api/dogs?page=1")
        max_page = result.json()["pagination"]["total_pages"]
        result2 = requests.get("https://www.doggiedb.me/api/dogs?page=" + str(max_page))
        assert result2.status_code == 200
        assert result2.json()["pagination"]["next_page"] == None

    def test_first_page(self):
        result = requests.get("https://www.doggiedb.me/api/dogs?page=1")
        assert result.json()["pagination"]["prev_page"] == None

    def test_dog_page_count(self):
        result = requests.get("https://www.doggiedb.me/api/dogs?page=1")
        page_count = result.json()["pagination"]["total_pages"]
        page_size = result.json()["pagination"]["per_page"]
        item_count = result.json()["pagination"]["total_items"]

        assert item_count/page_size <= page_count
        assert item_count/page_size > page_count - 1

    def test_breed_page_count(self):
        result = requests.get("https://www.doggiedb.me/api/breeds?page=1")
        page_count = result.json()["pagination"]["total_pages"]
        page_size = result.json()["pagination"]["per_page"]
        item_count = result.json()["pagination"]["total_items"]

        assert item_count/page_size <= page_count
        assert item_count/page_size > page_count - 1

    def test_ciy_page_count(self):
        result = requests.get("https://www.doggiedb.me/api/cities?page=1")
        page_count = result.json()["pagination"]["total_pages"]
        page_size = result.json()["pagination"]["per_page"]
        item_count = result.json()["pagination"]["total_items"]

        assert item_count/page_size <= page_count
        assert item_count/page_size > page_count - 1

    def test_shelter_page_count(self):
        result = requests.get("https://www.doggiedb.me/api/shelters?page=1")
        page_count = result.json()["pagination"]["total_pages"]
        page_size = result.json()["pagination"]["per_page"]
        item_count = result.json()["pagination"]["total_items"]

        assert item_count/page_size <= page_count
        assert item_count/page_size > page_count - 1

    def test_shelter_breeds_duplicates(self):
        result = requests.get("https://www.doggiedb.me/api/shelters?page=1")
        r = result.json()
        for shelter in r["items"]:
            breeds = shelter["available_breed_names"]
            for breed in breeds:
                assert breeds.count(breed) == 1

    def test_nearby(self):
        result = requests.get("https://www.doggiedb.me/api/nearby?lat=30&lon=-97&rad=15")
        #result = requests.get("http://127.0.0.1:5000/api/nearby?lat=30&lon=-97&rad=15")
        assert result.status_code == 200

    def test_nearby2(self):
        result = requests.get("https://www.doggiedb.me/api/nearby?city=lima&state=OH&rad=15")
        #result = requests.get("http://127.0.0.1:5000/api/nearby?city=lima&state=OH&rad=15")
        assert result.status_code == 200


if __name__ == "__main__":
    main()
