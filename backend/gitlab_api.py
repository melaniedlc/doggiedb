import requests
import json

#TEST FILE
#Writing code to query gitlab for About info

#TODO:
"""
1) Get all data from gitlab (pretty easy)
2) Figure out how to get data to the frontend (not so easy)
    - Will provide api calls for frontend to retrieve data
"""

users = {}
tot_commits = 0
tot_issues = 0

#No idea why Melanie's and Pratteka's name is her full name and not username
#valid_names = ["raneyj", "melanie de la cruz", "pat", "prateeka kodali", "sriramalagappan"]

#Each user has a list of aliases that gitlab uses to refer to them
valid_names = {
    "Jack Raney": ["raneyj", "jack raney"],
    "Melanie De La Cruz": ["melanie de la cruz", "melaniedlc"],
    "Patricio Rodriguez": ["pat", "patricio"],
    "Prateeka Kodali": ["prateeka kodali", "prateekakodali"],
    "Sriram Alagappan": ["sriramalagappan", "="]
}

#Stores the data for each user (also has fun printing formatting)
class user_data:
    commits = 0
    unit_tests = 0
    issues = 0

    def __init__(self):
        self.commits = 0
        self.unit_tests = 0
        self.issues = 0

    def __str__(self):
        return "commits: " + str(self.commits) + " unit tests: " + str(self.unit_tests) + " issues: " + str(self.issues)

    def __repr__(self):
        return "commits: " + str(self.commits) + " unit tests: " + str(self.unit_tests) + " issues: " + str(self.issues)

#Returns true if name is valid, false otherwise
#This is not the most efficient way to handle this, but our data
#structure is small and I want to move on
def get_valid_name(name):
    for entry in valid_names:
        if name in valid_names[entry]:
            return entry

    return ""

def refresh_commits():
    global tot_commits
    page_number = 1
    r = requests.get("https://gitlab.com/api/v4/projects/24704939/repository/commits?page=" + str(page_number))
    data = r.json()
    while data:
        tot_commits += len(data)
        i = iter(data)
        try:
            while True:
                entry = next(i)
                name = entry['author_name'].lower()
                #Convert to valid name
                v_name = get_valid_name(name)
                if v_name == "":
                    #invalid name
                    print(name)
                    continue
                if v_name in users.keys():
                    users[v_name].commits += 1
                else:
                    users[v_name] = user_data()
                    users[v_name].commits = 1
        except:
            pass
        # get next page of commits
        page_number += 1
        r = requests.get("https://gitlab.com/api/v4/projects/24704939/repository/commits?page=" + str(page_number))
        data = r.json()


def refresh_issues():
    global tot_issues
    page_number = 1
    r = requests.get("https://gitlab.com/api/v4/projects/24704939/issues?page=" + str(page_number))
    data = r.json()
    while data:
        tot_issues += len(data)
        i = iter(data)
        try:
            while True:
                entry = next(i)
                #Issues keep track of Username and Name variables
                #It seems inconsistent which of these Commits use, so we will check both
                name = entry['author']['username'].lower()
                v_name = get_valid_name(name)
                if v_name == "":
                    #invalid name
                    #print(name)
                    continue

                if v_name in users.keys():
                    users[v_name].issues += 1
                else:
                    users[v_name] = user_data()
                    users[v_name].issues = 1
        except:
            pass
        # get next page of issues
        page_number += 1
        r = requests.get("https://gitlab.com/api/v4/projects/24704939/issues?page=" + str(page_number))
        data = r.json()

#return about data in json format
def get_statistics():
    global users
    global tot_commits
    global tot_issues
    users = {}
    tot_commits = 0
    tot_issues = 0
    refresh_commits()
    refresh_issues()

    users_dict = {}
    for u in users.keys():
        u_dict = {
            "commits": users[u].commits,
            "issues": users[u].issues,
            "unit tests": users[u].unit_tests
        }
        users_dict[u] = u_dict


    data_dict = {
        "tot_commits": tot_commits,
        "tot_issues": tot_issues,
        "users": users_dict
    }

    print(json.dumps(data_dict))
    return json.dumps(data_dict)


if __name__ == "__main__":
    get_statistics()