from database import db
from database.models import City, Breed, Shelter, Dog
import requests
import json

def clear_empty_cities():
    print("SEARCHING")
    cities = City.query.filter(City.id > 5000).all()
    count = 0
    for city in cities:
        if len(city.shelters) == 0:
            count += 1
            print(city.name)
            db.session.delete(city)
            db.session.commit()

    print(str(count) + " Deleted")

def get_rid_of_canada():
    states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "HI", "ID", "IL",
    "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH",
    "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT",
    "VT", "VA", "VI", "WA", "WV", "WI", "WY"]

    #Go through cities. If state is not in states then we need to go through each shelter and delete all dogs from that shelter
    #then delete the shelter, then delete the city
    print("getting cities")
    cities = City.query.filter(City.id > 5000).all()
    print("got cities")
    for city in cities:
        if city.state not in states:
            print(city.name, city.state)
            for shelter in city.shelters:
                print("   -", shelter.name)
                for dog in shelter.dogs:
                    print("      -", dog.name)
                    #DELETE DOG
                    db.session.delete(dog)
                #DELETE SHELTER
                db.session.delete(shelter)
            #DELETE CITY
            db.session.delete(city)
    
    db.session.commit()

def build_relations():
    """
    #First lets do City <-> Shelters
    #for each shelter, use city name and state to find its city in cities then build relationship
    shelters = Shelter.query.filter_by(city_id=None).all()
    cities = City.query.all()
    #print(shelters)
    count = 0
    for shelter in shelters:
        #find city and add shelter to city.shelters
        _name = shelter.city_name.lower()
        _state = shelter.state
        filtered_city = [c for c in cities if c.name==_name and c.state==_state]
        #city = City.query.filter_by(name=_name, state=_state).first()
        if len(filtered_city) > 0:
            city = filtered_city[0]
            print("CITY FOUND for", shelter.name, shelter.city_name, shelter.state)
            city.shelters.append(shelter)
        else:
            print("NO CITY FOUND for", shelter.name, shelter.city_name, shelter.state)
        count += 1
        if count % 500 == 0:
            db.session.commit()
            print("COMMITING")

    db.session.commit()
    return
    """

    #Next, do Breed <-> Dogs
    #for each dog, use breed name to find its breed in breeds then build relationship
    #dogs = Dog.query.filter_by(breed_id=None).filter(Dog.id > 60000).all()
    #print("searching")
    #for dog in dogs:
    #    _name = dog.breed_primary
    #    breed = Breed.query.filter_by(name=_name).first()
    #    if breed:
            # WE FOUND IT, make the relationship
            # can I do this?
    #        if dog not in breed.dogs:
    #            breed.dogs.append(dog)
    #            db.session.commit()
    #            print("adding dog " + str(dog.id) + " to breed " + str(breed.name))
    #    else:
    #        print("NO BREED FOUND for", dog.id)
    #return

    # Next, do Shelter <-> Dogs
    # For each dog, uses shelter
    # dogs = Dog.query.filter_by(shelter_id=None).all()
    # shelters = Shelter.query.all()
    # print("GOT DATA")
    # count = 0
    # for dog in dogs:
    #     #find shelter and add dog to shelter.dogs
    #     _code = dog.shelter_name
    #     _code = _code[18:]
    #     print(_code)
    #     _code_str = "/v2/animals?organization=" + _code
    #     print(_code_str)
    #     filtered_shelter = [s for s in shelters if s.animals==_code_str]
    #     if len(filtered_shelter) > 0:
    #         shelter = filtered_shelter[0]
    #         print("SHELTER FOUND for", dog.name)
    #         shelter.dogs.append(dog)
    #     else:
    #         print("NO SHELTER FOUND for", dog.name)
    #     count += 1
    #     if count % 500 == 0:
    #         db.session.commit()
    #         print("COMITTING")

    # db.session.commit()
    # return

    # Next do many-to-many Breeds <-> Shelters
    # For each shelter, go through dogs and add dog.breed to shelter.breeds



    ########################################################################################
    #print("loading shelters")
    #shelters = (
    #    Shelter.query.options(db.joinedload(Shelter.breeds))
    #    .options(db.joinedload(Shelter.dogs).joinedload(Dog.breed))
        #.filter(db.not_(Shelter.breeds.any()))
    #    .filter(Shelter.id >= 10000).all()
    #)

    #print("got shelters")
    #sheltercount = len(shelters)
    #count = 0
    #for shelter in shelters:
    #    print(f"{count}/{sheltercount}")

    #    for dog in shelter.dogs:
            # this runs another query, so we have objects in memory now
    #       shelterBreeds = [x for x in shelter.breeds]

            # If the breed is not already in shelter.breeds, add it
    #        if dog.breed is not None and dog.breed not in shelterBreeds:
    #            shelter.breeds.append(dog.breed)
    #            print(f"added {dog.breed} to {shelter}")
    #    count += 1
    #    if count % 100 == 0:
    #        db.session.commit()
    #        print("COMITTING")

    #db.session.commit()
    #return

    # Next do many-to-many Breeds <-> Cities
    # For each city, go through shelters and add shelter.breeds to city.breeds
    print("loading cities")
    cities = (
        City.query
        .options(db.joinedload(City.breeds))
        .options(db.joinedload(City.shelters).joinedload(Shelter.breeds))
        #.filter(db.not_(City.breeds.any()))
        .filter(City.id > 5000)
        .all()
    )
    citycount = len(cities)
    count = 0
    for city in cities:
        print(f"{count}/{citycount}")

        for shelter in city.shelters:
            for breed in shelter.breeds:
                # for cityBreed in city.breeds:
                #     if breed is cityBreed:
                #         break
                citybreeds = [x for x in city.breeds]
                if breed not in citybreeds:
                    city.breeds.append(breed)
                    print(f"added {breed} to {city}")
        count += 1
        if count % 100 == 0:
            db.session.commit()
            print("COMITTING")

    db.session.commit()
    return