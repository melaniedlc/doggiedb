import React, { useState, useEffect } from 'react';
import { Table, Button, Accordion, Card, Row, Col, Container, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { fetchCities, fetchProximitySearch, usePaginationState } from '../../../api';
import LoadingModal from '../../loading_modal/LoadingModal';
import { Paginator } from '../../paginator';
import { DataTable } from '../../datatable';
import qs from 'qs';

export const ProximityPage = ({ location, history }) => {
    const [nearbyParams, setNearbyParams] = useState({ lat: '', lon: '', city: '', state: '', rad: '' });
    const [isLoading, setIsLoading] = useState(true);
    const [apiResponse, setApiResponse] = usePaginationState();

    useEffect(() => {
        document.title = 'Proximity';

        const query = qs.parse(location.search.slice(1));

        const params = { lat: '', lon: '', city: '', state: '', rad: '' };
        query.lat && (params.lat = query.lat);
        query.lon && (params.lon = query.lon);
        query.city && (params.city = query.city);
        query.state && (params.state = query.state);
        query.rad && (params.rad = query.rad);

        setNearbyParams(params);
        // setFilter(query.filter ?? {});
        // setSort(query.sort ?? {});
        setIsLoading(true);

        fetchProximitySearch(params).then((response) => {
            setApiResponse(response);
            setIsLoading(false);
        });
    }, [location]);

    const searchCity = () => {
        history.push({
            pathname: '/nearby',
            search: `?${qs.stringify({ ...nearbyParams, lat: undefined, lon: undefined })}`,
        });
    };

    const searchCoords = () => {
        history.push({
            pathname: '/nearby',
            search: `?${qs.stringify({ ...nearbyParams, city: undefined, state: undefined })}`,
        });
    };

    return (
        <div>
            <LoadingModal show={isLoading} />

            <h2>search</h2>

            <Container fluid={true}>
                <Row>
                    <Col>
                        <Accordion defaultActiveKey="0">
                            <Card>
                                <Card.Header>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                        nearby a city
                                    </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="0">
                                    <Card.Body>
                                        <Form.Control
                                            type="text"
                                            placeholder="city"
                                            value={nearbyParams.city}
                                            onChange={(event) => {
                                                const np = { ...nearbyParams };
                                                np.city = event.currentTarget.value;
                                                setNearbyParams(np);
                                            }}
                                        />
                                        <Form.Control
                                            type="text"
                                            placeholder="state"
                                            value={nearbyParams.state}
                                            onChange={(event) => {
                                                const np = { ...nearbyParams };
                                                np.state = event.currentTarget.value;
                                                setNearbyParams(np);
                                            }}
                                        />
                                        <Form.Control
                                            type="text"
                                            placeholder="radius in miles"
                                            value={nearbyParams.rad}
                                            onChange={(event) => {
                                                const np = { ...nearbyParams };
                                                np.rad = event.currentTarget.value;
                                                setNearbyParams(np);
                                            }}
                                        />
                                        <Button className="d-block w-100" onClick={searchCity}>
                                            search
                                        </Button>
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                            <Card>
                                <Card.Header>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="1">
                                        nearby coordinates
                                    </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="1">
                                    <Card.Body>
                                        <Form.Control
                                            type="text"
                                            placeholder="latitude"
                                            value={nearbyParams.lat}
                                            onChange={(event) => {
                                                const np = { ...nearbyParams };
                                                np.lat = event.currentTarget.value;
                                                setNearbyParams(np);
                                            }}
                                        />
                                        <Form.Control
                                            type="text"
                                            placeholder="longitude"
                                            value={nearbyParams.lon}
                                            onChange={(event) => {
                                                const np = { ...nearbyParams };
                                                np.lon = event.currentTarget.value;
                                                setNearbyParams(np);
                                            }}
                                        />
                                        <Form.Control
                                            type="text"
                                            placeholder="radius in miles"
                                            value={nearbyParams.rad}
                                            onChange={(event) => {
                                                const np = { ...nearbyParams };
                                                np.rad = event.currentTarget.value;
                                                setNearbyParams(np);
                                            }}
                                        />
                                        <Button className="d-block w-100" onClick={searchCoords}>
                                            search
                                        </Button>
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                        </Accordion>
                    </Col>
                </Row>
            </Container>

            {apiResponse.items.map((city) => {
                return (
                    <a href={`cities/${city.id}`} target="_blank">
                        <h1>
                            {city.name}, {city.state}
                        </h1>
                    </a>
                );
            })}
        </div>
    );
};
