import React, { useState, useEffect } from 'react';
import { Stat } from '../../Stat';
import { fetchDog } from '../../../api';
import { Image } from 'react-bootstrap';
import LoadingModal from "../../loading_modal/LoadingModal";
import { GiHealthNormal, FaWeightHanging, BsArrowUpDown, FaDog, BsCheck, BsX, BsQuestion, BsXSquareFill } from 'react-icons/all';
import dogpic from '../../../static_resources/doglogo.jpg';
import Map from '../../map/Map';

export const DogInstance = (props) => {
    const [dog, setDog] = useState({});
    const [location, setLocation] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    var MapComponent;

    useEffect(() => {
        // Default title
        document.title = "Dog"

        setIsLoading(true)

        fetchDog(props.match.params.id).then((response) => setDog(response))

    }, []);

    // if data loaded, turn off loading modal
    useEffect(() => {
        if (Object.keys(dog).length !== 0) {
            setIsLoading(false);
            // set title to breed name
            document.title = dog.name
            setLocation({ lat: dog.city?.latitude, lng: dog.city?.longitude });
        }
    }, [dog]);


    return (
        <div class="pt-5 pb-5">
            <LoadingModal show={isLoading} />
            <div class="row justify-content-between">
                <div class="col col-md-12 pr-md-5 pl-md-5 mt-5">
                    <div class="mb-3 ml-3">
                        <h1><b>{dog.name}</b></h1>
                        <h3>Shelter: <a href={'../shelters/' + dog.shelter_id}>{dog.shelter?.name}</a></h3>
                        <h3>City: <a href={'../cities/' + dog.shelter?.city_id}>{dog.shelter?.city_name}</a>, {dog.shelter?.state}</h3>
                        { dog.breed_id && <h3>Breed: <a href={'../breeds/' + dog.breed_id}>{dog.breed_primary}</a> </h3>}
                        { dog.breed_id == null && <h3>Breed: {dog.breed_primary}</h3>}
                    </div>
                    <div class="row align-items-center justify-content-between">
                        <div class="col-12 col-md-6  text-left">
                            <div class="mb-3 ml-3">
                                <div class="my-3 card card-body bg-primary text-light shadow p-4 "> 
                                    {/* { dog.breedId && <h3>Breed: <a href={'../breeds/' + dog.breedId}>{dog.breed}</a> </h3>}
                                    { dog.breedId == null && <h3>Breed: {dog.breed}</h3>} */}
                                    <h3> Age: {dog.age} </h3>
                                    <h3> Gender: {dog.gender} </h3>
                                    <h3> Size: {dog.size} </h3>
                                    { dog.color && <h3> Color: {dog.color_primary} </h3> }
                                    {/* <h3><a href={dog.link}>{"Visit online"}</a></h3> */}
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="my-3 card card-body shadow p-4 ">
                                            <div class="row align-items-center d-flex text-md-center text-lg-left">
                                                <div class="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div class="icon-wrap text-primary my-3">
                                                    { dog.house_trained == null && <BsQuestion size={30} /> }
                                                    { dog.house_trained == 1 && <BsCheck size={30} /> }
                                                    { dog.house_trained == 0 && <BsX size={30} /> }
                                                    {/* {dog.house ? <BsCheck size={30} /> : <BsX size={30} />} */}
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-9 mt-3 mt-lg-0">
                                                    <h6 class="font-weight-bold">House Trained</h6>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="my-3 card card-body shadow p-4 ">
                                            <div class="row align-items-center d-flex text-md-center text-lg-left">
                                            <div class="col-12 col-sm-3 col-md-3 text-center px-0">
                                                <div class="icon-wrap text-primary my-3">
                                                    { dog.spayed_neutered == null && <BsQuestion size={30} /> }
                                                    { dog.spayed_neutered == 1 && <BsCheck size={30} /> }
                                                    { dog.spayed_neutered == 0 && <BsX size={30} /> }
                                                    {/* {dog.spayed_neutered ? <BsCheck size={30} /> : <BsX size={30} />} */}
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-9 mt-3 mt-lg-0">
                                                <h6 class="font-weight-bold">Spayed / Neutered</h6>
                                            </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col">
                                        <div class="my-3 card card-body shadow p-4 ">
                                            <div class="row align-items-center d-flex text-md-center text-lg-left">
                                            <div class="col-12 col-sm-3 col-md-3 text-center px-0">
                                                <div class="icon-wrap text-primary my-3">
                                                    { dog.special_needs == null && <BsQuestion size={30} /> }
                                                    { dog.special_needs == 1 && <BsCheck size={30} /> }
                                                    { dog.special_needs == 0 && <BsX size={30} /> }
                                                    {/* {dog.special ? <BsCheck size={30} /> : <BsX size={30} />} */}
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-9 mt-3 mt-lg-0">
                                                <h6 class="font-weight-bold">Special Needs</h6>
                                                {/* <p class=" mb-0">{dog.gender}</p> */}
                                            </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="my-3 card card-body shadow p-4 ">
                                            <div class="row align-items-center d-flex text-md-center text-lg-left">
                                                <div class="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div class="icon-wrap text-primary my-3">
                                                        { dog.good_children == null && <BsQuestion size={30} /> }
                                                        { dog.good_children == 1 && <BsCheck size={30} /> }
                                                        { dog.good_children == 0 && <BsX size={30} /> }
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-9 mt-3 mt-lg-0">
                                                    <h6 class="font-weight-bold">Good with Kids</h6>
                                                    <p class=" mb-0">{dog.child}</p>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="my-3 card card-body shadow p-4 ">
                                            <div class="row align-items-center d-flex text-md-center text-lg-left">
                                            <div class="col-12 col-sm-3 col-md-3 text-center px-0">
                                                <div class="icon-wrap text-primary my-3">
                                                    { dog.good_dogs == null && <BsQuestion size={30} /> }
                                                    { dog.good_dogs == 1 && <BsCheck size={30} /> }
                                                    { dog.good_dogs == 0 && <BsX size={30} /> }
                                                    {/* {dog.otherDogs ? <BsCheck size={30} /> : <BsX size={30} />} */}
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-9 mt-3 mt-lg-0">
                                                <h6 class="font-weight-bold">Good with Other Dogs</h6>
                                                {/* <p class=" mb-0">{dog.gender}</p> */}
                                            </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col">
                                        <div class="my-3 card card-body shadow p-4 ">
                                            <div class="row align-items-center d-flex text-md-center text-lg-left">
                                            <div class="col-12 col-sm-3 col-md-3 text-center px-0">
                                                <div class="icon-wrap text-primary my-3">
                                                    { dog.good_cats == null && <BsQuestion size={30} /> }
                                                    { dog.good_cats == 1 && <BsCheck size={30} /> }
                                                    { dog.good_cats == 0 && <BsX size={30} /> }
                                                    {/* {dog.cats ? <BsCheck size={30} /> : <BsX size={30} />} */}
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-9 mt-3 mt-lg-0">
                                                <h6 class="font-weight-bold">Good with Cats</h6>
                                                {/* <p class=" mb-0">{dog.gender}</p> */}
                                            </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mb-4 ml-md-auto">
                            {/* {dog.image &&  */}
                            <img alt="dog-image" class="shadow" 
                                src={dog.photo ? dog.photo : dogpic} style={{ borderRadius: 10 }} /> 
                                {/* } */}
                            <h3><a href={dog.pet_finder}>{"Visit online"}</a></h3>
                        </div>
                    </div>
                    <div class="mt-3">
                        <Map location={location} zoomLevel={5} />
                    </div>
                </div>
            </div>   
        </div>
    );
};

export default DogInstance;