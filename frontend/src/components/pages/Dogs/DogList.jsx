import React, { useState, useEffect } from 'react';
import { Table, Button, Container, Row, Col, Card, Form, Dropdown, DropdownButton } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { fetchDogs } from '../../../api';
import { usePaginationState, fetchBreeds } from '../../../api';
import { Paginator } from '../../paginator';
import qs from 'qs';
import dogpic from '../../../static_resources/doglogo.jpg';

export const DogList = ({ location, history }) => {
    const [rows, setRows] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [apiResponse, setApiResponseInternal] = usePaginationState();
    const [filter, setFilter] = useState({});
    const [sort, setSort] = useState({});

    const columns={
        name: { display: 'name' },
        breed_primary: { display: 'breed' },
        age: { display: 'age' },
        gender: { display: 'gender' },
        size: { display: 'size' },
    };
 
    const setApiResponse = (response) => {
        setApiResponseInternal(response);
        const chunk = 6;
        const rows = [];
        for (let i = 0, j = response.items.length; i < j; i += chunk) {
            rows.push(response.items.slice(i, i + chunk));
        }
        setRows(rows);
    };

    const changePage = (page) => {
        history.push({
            pathname: '/dogs',
            search: `?${qs.stringify({ page, filter, sort })}`,
        });
    };

    useEffect(() => {
        document.title = 'Dogs';

        const query = qs.parse(location.search.slice(1));

        setIsLoading(true);
        setFilter(query.filter ?? {});
        setSort(query.sort ?? {});
        fetchDogs(query.page ?? 1, query.filter ?? {}, query.sort ?? {}).then((response) => {
            setApiResponse(response);
            setIsLoading(false);
        });
    }, [location]);

    const textInput = React.useRef();
 
    function onFilterApply() {
        history.push({
            pathname: '/dogs',
            search: `?${qs.stringify({ filter, sort })}`
        })
    }
 
    function onFilterChange(column, value) {
        const f = { ...filter };
        f[column] = value;
        setFilter(f);
    }

    function nameFilterChange(column, value) {
        var val = value == "a" ? "i" : undefined;
        const fVal = filter?.column ?? [];
        const f = [...fVal];
        if (value == "j")
            val = "r";
        else if(value == "s")
            val = "z";
        f[0] = value ? value : undefined;
        f[1] = val ? val : undefined;
        onFilterChange(column, f);
    }

    function onSort(column, ascending) {
        const s = {};
        s[column] = ascending ? 'asc' : 'desc';
        history.push({
            pathname: '/dogs',
            search: `?${qs.stringify({ filter, sort: s })}`,
        });
    }
 
    return (
        <div>
            <h1 className="text-align center">Dogs</h1>
            <div class="m-3">
                <Button variant="success" as={Link} to={'/search-dogs'}>Search for dogs</Button>
            </div>

           <Form>
                <Form.Row>
                    <Col>
                        <Form.Control as="select" 
                            onChange={(event) => nameFilterChange('name', event.currentTarget.value ? event.currentTarget.value : undefined)}
                        >
                            <option value="">Filter by Name</option>
                            <option value="a">A-I</option>
                            <option value="j">J-R</option>
                            <option value="s">S-Z</option>
                        </Form.Control>
                    </Col>
                    <Col>
                        <Form.Control as="select" 
                            onChange={(event) => onFilterChange('breed_primary', event.currentTarget.value ? event.currentTarget.value : undefined)}
                        >
                            <option value="">Filter by Breed</option>
                            <option value="affenpinscher">Affenpinscher</option>
                            <option value="afghan hound">Afghan Hound</option>
                            <option value="african hunting dog">African Hunting Dog</option>
                            <option value="airedale terrier">Airedale Terrier</option>
                            <option value="akbash dog">Akbash Dog</option>
                            <option value="akita">Akita</option>
                            <option value="alapaha blue blood bulldog">Alapaha Blue Blood Bulldog</option>
                            <option value="alaskan husky">Alaskan Husky</option>
                            <option value="alaskan malamute">Alaskan Malamute</option>
                            <option value="american bulldog">American Bulldog</option>
                            <option value="american bully">American Bully</option>
                            <option value="american eskimo dog">American Eskimo Dog</option>
                            <option value="american eskimo dog (miniature)">Ameriican Eskimo Dog (Miniature)</option>
                            <option value="american foxhound">American Foxhound</option>
                            <option value="american staffordshire terrier">American Staffordhire Terrier</option>
                            <option value="american water spaniel">American Water Spaniel</option>
                            <option value="anatolian shepherd dog">Anatolian Shepherd Dog</option>
                            <option value="appenzeller sennenhund">Appenzeller Sennenhund</option>
                            <option value="australian cattle dog">Australian Cattle Dog</option>
                            <option value="australian kelpie">Australian Kelpie</option>
                            <option value="australian shepherd">Australian Shepherd</option>
                            <option value="australian terrier">Australian Terrier</option>
                            <option value="azawakh">Azawakh</option>
                            <option value="barbet">Barbet</option>
                            <option value="basenji">Basenji</option>
                            <option value="basset bleu de gascogne">Basset Bleu de Gascogne</option>
                            <option value="basset hound">Basset Hound</option>
                            <option value="beagle">Beagle</option>
                            <option value="bearded collie">Bearded Collie</option>
                            <option value="beauceron">Beauceron</option>
                            <option value="bedlington terrier">Bedlington Terrier</option>
                            <option value="belgian malinois">Belgian Malinois</option>
                            <option value="belgian tervuren">Belgian Tervuren</option>
                            <option value="bernese mountain dog">Bernese Mountain Dog</option>
                            <option value="bichon frise">Bichon Frise</option>
                            <option value="black and tan coonhound">Black and Tan Coonhound</option>
                            <option value="bloodhound">Bloodhound</option>
                            <option value="bluetick coonhound">Bluetick Coonhound</option>
                            <option value="boerboel">Boerboel</option>
                            <option value="border collie">Border Collie</option>
                            <option value="border terrier">Border Terrier</option>
                            <option value="boston terrier">Boston Terrier</option>
                            <option value="bouvier des flandres">Bouvier de Flandres</option>
                            <option value="boxer">Boxer</option>
                            <option value="boykin spaniel">Boykin Spaniel</option>
                            <option value="bracco italiano">Bracco Italiano</option>
                            <option value="briard">Briard</option>
                            <option value="brittany">Brittany</option>
                            <option value="bull terrier">Bull Terrier</option>
                            <option value="bull terrier (miniature)">Bull Terrier (Miniature)</option>
                            <option value="bullmastiff">Bullmastiff</option>
                            <option value="cairn terrier">Cairn Terrier</option>
                            <option value="cane corso">Cane Corso</option>
                            <option value="cardigan welsh corgi">Cardigan Welsh Corgi</option>
                            <option value="catahoula leopard dog">Catahoula Leopard Dog</option>
                            <option value="caucasian shepherd (ovcharka)">Caucasian Shepherd (Ovcharka)</option>
                            <option value="cavalier king charles spaniel">Cavalier King Charles Spaniel</option>
                            <option value="chesapeake bay retriever">Chesapeake Bay Retriever</option>
                            <option value="chihuahua">Chihuahua</option>
                            <option value="chinese crested">Chinese Crested</option>
                            <option value="chinese shar-pei">Chinese Shar-Pei</option>
                            <option value="chinook">Chinook</option>
                            <option value="chow chow">Chow Chow</option>
                            <option value="clumber spaniel">Clumber Spaniel</option>
                            <option value="cocker spaniel">Cocker Spaniel</option>
                            <option value="cocker spaniel (american)">Cocker Spaniel (American)</option>
                            <option value="coton de tulear">Coton de Tulear</option>
                            <option value="dachshund">Dachshund</option>
                            <option value="dalmatian">Dalmatian</option>
                            <option value="doberman pinscher">Doberman Pinscher</option>
                            <option value="dogo argentino">Dogo Argentino</option>
                            <option value="dutch shepherd">Dutch Shepherd</option>
                            <option value="english bulldog">English Bulldog</option>
                            <option value="english setter">English Setter</option>
                            <option value="english shepherd">English Shepherd</option>
                            <option value="english springer spaniel">English Springer Spaniel</option>
                            <option value="english toy spaniel">English Toy Spaniel</option>
                            <option value="english toy terrier">English Toy Terrier</option>
                            <option value="eurasier">Eurasier</option>
                            <option value="field spaniel">Field Spaniel</option>
                            <option value="finnish lapphund">Finnish Lapphund</option>
                            <option value="finnish spitz">Finnish Spitz</option>
                            <option value="french bulldog">French Bulldog</option>
                            <option value="german pinscher">German Pinscher</option>
                            <option value="german shepherd dog">German Shepherd Dog</option>
                            <option value="german shorthaired pointer">German Shorthaired Pointer</option>
                            <option value="giant schnauzer">Giant Schnauzer</option>
                            <option value="glen of imaal terrier">Glen of Imaal Terrier</option>
                            <option value="golden retriever">Golden Retriever</option>
                            <option value="gordon setter">Gordon Setter</option>
                            <option value="great dane">Great Dane</option>
                            <option value="great pyrenees">Great Pyrenees</option>
                            <option value="greyhound">Greyhound</option>
                            <option value="griffon bruxellois">Griffon Bruxellois</option>
                            <option value="harrier">Harrier</option>
                            <option value="havanese">Havanese</option>
                            <option value="irish setter">Irish Setter</option>
                            <option value="irish terrier">Irish Terrier</option>
                            <option value="irish wolfhound">Irish Wolfhound</option>
                            <option value="italian greyhound">Italian Greyhound</option>
                            <option value="japanese chin">Japanese Chin</option>
                            <option value="japanese spitz">Japanese Spitz</option>
                            <option value="keeshond">Keeshond</option>
                            <option value="komondor">Komondor</option>
                            <option value="kooikerhondje">Kooikerhondje</option>
                            <option value="kuvasz">Kuvasz</option>
                            <option value="labrador retriever">Labrador Retriever</option>
                            <option value="lagotto romagnolo">Lagotto Romagnolo</option>
                            <option value="lancashire heeler">Lancashire Heeler</option>
                            <option value="leonberger">Leonberger</option>
                            <option value="lhasa apso">Lhasa Apso</option>
                            <option value="maltese">Maltese</option>
                            <option value="miniature american shepherd">Miniature American Shepherd</option>
                            <option value="miniature pinscher">Miniature Pinscher</option>
                            <option value="miniature schnauzer">Miniature Schnauzer</option>
                            <option value="newfoundland">Newfoundland</option>
                            <option value="norfolk terrier">Norfolk Terrier</option>
                            <option value="norwich terrier">Norwich Terrier</option>
                            <option value="nova scotia duck tolling retriever">Nova Scotia Duck Tolling Retriever</option>
                            <option value="old english sheepdog">Old English Sheepdog</option>
                            <option value="papillon">Papillon</option>
                            <option value="pekingese">Pekingese</option>
                            <option value="pembroke welsh corgi">Pembroke Welsh Corgi</option>
                            <option value="perro de presa canario">Perro de Presa Canario</option>
                            <option value="pharaoh hound">Pharaoh Hound</option>
                            <option value="pit bull terrier">Pit Bull Terrier</option>
                            <option value="plott">Plott</option>
                            <option value="pomeranian">Pomeranian</option>
                            <option value="poodle (miniature)">Poodle (Miniature)</option>
                            <option value="poodle (toy)">Poodle (Toy)</option>
                            <option value="pug">Pug</option>
                            <option value="puli">Puli</option>
                            <option value="pumi">Pumi</option>
                            <option value="rat terrier">Rat Terrier</option>
                            <option value="redbone coonhound">Redbone Coonhound</option>
                            <option value="rhodesian ridgeback">Rhodesian Ridgeback</option>
                            <option value="rottweiler">Rottweiler</option>
                            <option value="russian toy">Russian Toy</option>
                            <option value="saint bernard">Saint Bernard</option>
                            <option value="saluki">Saluki</option>
                            <option value="samoyed">Samoyed</option>
                            <option value="schipperke">Schipperke</option>
                            <option value="scottish deerhound">Scottish Deerhound</option>
                            <option value="scottish terrier">Scottish Terrier</option>
                            <option value="shetland sheepdog">Shetland Sheepdog</option>
                            <option value="shiba inu">Shiba Inu</option>
                            <option value="shih tzu">Shih Tzu</option>
                            <option value="shiloh shepherd">Shiloh Shepherd</option>
                            <option value="siberian husky">Siberian Husky</option>
                            <option value="silky terrier">Silky Terrier</option>
                            <option value="smooth fox terrier">Smooth Fox Terrier</option>
                            <option value="soft coated wheaten terrier">Soft Coated Wheaten Terrier</option>
                            <option value="spanish water dog">Spanish Water Dog</option>
                            <option value="spinone italiano">Spinone Italiano</option>
                            <option value="staffordshire bull terrier">Staffordshire Bull Terrier</option>
                            <option value="standard schnauzer">Standard Schnauzer</option>
                            <option value="swedish vallhund">Swedish Vallhund</option>
                            <option value="thai ridgeback">Thai Ridgeback</option>
                            <option value="tibetan mastiff">Tibetan Mastiff</option>
                            <option value="tibetan spaniel">Tibetan Spaniel</option>
                            <option value="tibetan terrier">Tibetan Terrier</option>
                            <option value="toy fox terrier">Toy Fox Terrier</option>
                            <option value="treeing walker coonhound">Treeing Walker Coonhound</option>
                            <option value="vizsla">Vizsla</option>
                            <option value="weimaraner">Weimaraner</option>
                            <option value="welsh springer spaniel">Welsh Springer Spaniel</option>
                            <option value="west highland white terrier">West Highland White Terrier</option>
                            <option value="whippet">Whippet</option>
                            <option value="white shepherd">White Shepherd</option>
                            <option value="wire fox terrier">Wire Fox Terrier</option>
                            <option value="wirehaired pointing griffon">Wirehaired Pointing Griffon</option>
                            <option value="wirehaired vizsla">Wirehaired Vizsla</option>
                            <option value="xoloitzcuintli">Xoloitzcuintli</option>
                            <option value="yorkshire terrier">Yorkshire Terrier</option>
                        </Form.Control>
                    </Col>
                    <Col>
                        <Form.Control as="select" 
                            onChange={(event) => onFilterChange('age', event.currentTarget.value ? event.currentTarget.value : undefined)}
                        >
                            <option value="">Filter by Age</option>
                            <option value="baby">Baby</option>
                            <option value="young">Young</option>
                            <option value="adult">Adult</option>
                            <option value="senior">Senior</option>
                        </Form.Control>
                    </Col>
                    <Col>
                        <Form.Control as="select" 
                            onChange={(event) => onFilterChange('gender', event.currentTarget.value ? event.currentTarget.value : undefined)}
                        >
                            <option value="">Filter by Gender</option>
                            <option value="female">Female</option>
                            <option value="male">Male</option>
                        </Form.Control>
                    </Col>
                    <Col>
                        <Form.Control as="select" 
                            onChange={(event) => onFilterChange('size', event.currentTarget.value ? event.currentTarget.value : undefined)}
                        >
                            <option value="">Filter by Size</option>
                            <option value="small">Small</option>
                            <option value="medium">Medium</option>
                            <option value="large">Large</option>
                            <option value="extra large">Extra Large</option>
                        </Form.Control>
                    </Col>
                    <Col>
                        <DropdownButton id="dropdown-basic-button" title="Sort by" variant="secondary">
                            <Dropdown.Item onClick={(e) => {
                                e.preventDefault();
                                onSort('name', true);
                            }}>
                                Name (asc)
                            </Dropdown.Item>
                            <Dropdown.Item onClick={(e) => {
                                e.preventDefault();
                                onSort('name', false);
                            }}>Name (desc)</Dropdown.Item>
                            <Dropdown.Item onClick={(e) => {
                                e.preventDefault();
                                onSort('breed_primary', true);
                            }}>Breed (asc)</Dropdown.Item>
                            <Dropdown.Item onClick={(e) => {
                                e.preventDefault();
                                onSort('breed_primary', false);
                            }}>Breed (desc)</Dropdown.Item>
                            <Dropdown.Item onClick={(e) => {
                                e.preventDefault();
                                onSort('age', true);
                            }}>Age (asc)</Dropdown.Item>
                            <Dropdown.Item onClick={(e) => {
                                e.preventDefault();
                                onSort('age', false);
                            }}>Age (desc)</Dropdown.Item>
                            <Dropdown.Item onClick={(e) => {
                                e.preventDefault();
                                onSort('gender', true);
                            }}>Gender (asc)</Dropdown.Item>
                            <Dropdown.Item onClick={(e) => {
                                e.preventDefault();
                                onSort('gender', false);
                            }}>Gender (desc)</Dropdown.Item>
                            <Dropdown.Item onClick={(e) => {
                                e.preventDefault();
                                onSort('size', true);
                            }}>Size (asc)</Dropdown.Item>
                            <Dropdown.Item onClick={(e) => {
                                e.preventDefault();
                                onSort('size', false);
                            }}>Size (desc)</Dropdown.Item>
                        </DropdownButton>
                    </Col>
                </Form.Row>
            </Form>
            <br />
            <Button className="d-block w-100" onClick={onFilterApply}>apply filters</Button>
            <br />
            <Container>
                {rows.map((cols) => (
                    <Row>
                        {cols.map((dog, i) => (
                            <Col className="col-sm-4 py-2">
                                <Card style={{ width: '18rem' }}>
                                    <Card.Img variant="top" height="300rem" src={dog.photo ? dog.photo : dogpic} />
                                    <Card.Body>
                                        <a href={'/dogs/' + dog.id}>
                                            <Card.Title>{dog.name}</Card.Title>
                                        </a>
                                        <Card.Text>
                                            <p>
                                                <b>Breed: </b>
                                                {dog.breed_primary} <br />
                                                <b>Age: </b>
                                                {dog.age} <br />
                                                <b>Gender: </b>
                                                {dog.gender} <br />
                                                <b>Size: </b>
                                                {dog.size} <br />
                                            </p>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        ))}
                    </Row>
                ))}
            </Container>
            <div className="d-flex justify-content-center">
                <Paginator onPageChange={changePage} pagination={apiResponse.pagination} />
            </div>
        </div>
    );
};
 
export default DogList;