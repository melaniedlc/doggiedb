import React, { useEffect, useState } from 'react';
import { fetchShelter } from '../../../api';
import { Stat } from '../../Stat';
import { Image } from 'react-bootstrap';
import LoadingModal from '../../loading_modal/LoadingModal';
import { MdLocationCity, ImPhone, ImListNumbered, MdEmail, RiComputerFill } from 'react-icons/all';
import { Container, Table, Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Map from '../../map/Map';
import { TableWithBrowserPagination, Column } from 'react-rainbow-components';

export const ShelterInstance = (props) => {
    const [shelter, setShelter] = useState({});
    const [location, setLocation] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    var MapComponent;

    useEffect(() => {
        // Default title
        document.title = 'Shelter';

        setIsLoading(true);

        fetchShelter(props.match.params.id).then((response) => setShelter(response));
    }, []);

    // if data loaded, turn off loading modal
    useEffect(() => {
        if (Object.keys(shelter).length !== 0) {
            setIsLoading(false);
            // set title to shelter name
            document.title = shelter.name;
            setLocation({ lat: shelter.city?.latitude, lng: shelter.city?.longitude });
        }
    }, [shelter]);

    const BreedButton = (breed) => <Button variant="success" as={Link} to={`/breeds/${breed.row.id}`}>View</Button>
    const DogButton = (dog) => <Button variant="success" as={Link} to={`/dogs/${dog.row.id}`}>View</Button>

    return (
        <div className="pt-5 pb-5">
            <LoadingModal show={isLoading} />
            <div className="row justify-content-between">
                <div className="col col-md-12 pr-md-5 pl-md-5 mt-5">
                    <div className="row align-items-center justify-content-between">
                        <div className="col-12 col-md-5  text-left">
                            <div className="mb-3 ml-3">
                                <h2>
                                    <b>{shelter.name}</b>
                                </h2>
                            </div>
                            <div className="mb-3 ml-3">
                                <div className="row">
                                    <div className="col">
                                        <div className="my-3 card card-body shadow p-4">
                                            <div className="row align-items-center d-flex text-md-center text-lg-left">
                                                <div className="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div className="icon-wrap text-primary my-3">
                                                        <MdLocationCity size={30} />
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-9 mt-3 mt-lg-0 ">
                                                    <h6 className="font-weight-bold">Location</h6>
                                                    <p className=" mb-0"><a href={'../cities/' + shelter.city?.id}>{shelter.city_name}, {shelter.state}</a></p>
                                                    <p className="mb-0">{shelter.address && shelter.address}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="my-3 card card-body shadow p-4 ">
                                            <div className="row align-items-center d-flex text-md-center text-lg-left">
                                                <div className="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div className="icon-wrap text-primary my-3">
                                                        <ImPhone size={30} />
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-9 mt-3 mt-lg-0">
                                                    <h6 className="font-weight-bold">Phone</h6>
                                                    <p className="mb-0">{shelter.phone ?? '(none on record)'}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="my-3 card card-body shadow p-4 ">
                                            <div className="row align-items-center d-flex text-md-center text-lg-left">
                                                <div className="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div className="icon-wrap text-primary my-3">
                                                        <MdEmail size={30} />
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-9 mt-3 mt-lg-0">
                                                    <h6 className="font-weight-bold">Email</h6>
                                                    <p className="mb-0">{shelter.email ?? '(none on record)'}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="my-3 card card-body shadow p-4 ">
                                            <div className="row align-items-center d-flex text-md-center text-lg-left">
                                                <div className="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div className="icon-wrap text-primary my-3">
                                                        <RiComputerFill size={30} />
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-9 mt-3 mt-lg-0">
                                                    <h6 className="font-weight-bold">Website</h6>
                                                    <p className="mb-0"><a target="_blank" href={shelter.website ? shelter.website : shelter.pet_finder}>{shelter.website ? shelter.website : shelter.pet_finder}</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-md-5 mb-4 ml-md-auto">
                            {shelter.photo && <img alt="shelter-image" className="shadow" src={shelter.photo} style={{ borderRadius: 10 }} />}
                        </div>
                    </div>
                    <div class="mt-3">
                        <Map location={location} zoomLevel={5} />
                    </div>
                </div>
            </div>

            <Container fluid={true}>
                <Row>
                    <Col className="mx-0">
                        <h3>Breeds</h3>
                        <Card>
                            <TableWithBrowserPagination
                                className="breeds"
                                keyField="id"
                                isLoading={isLoading}
                                data={shelter.breeds}
                                pageSize={5}
                            >
                                <Column header="Name" field="name" />
                                <Column header="Weight" field="weight" />
                                <Column header="Height" field="height" />
                                <Column header="View" field="view" component={BreedButton} />
                            </TableWithBrowserPagination>
                        </Card>
                    </Col>
                    <Col className="mx-0">
                        <h3>Dogs</h3>
                        <Card>
                            <TableWithBrowserPagination
                                className="dogs"
                                keyField="id"
                                isLoading={isLoading}
                                data={shelter.dogs}
                                pageSize={5}
                            >
                                <Column header="Name" field="name" />
                                <Column header="Breed" field="breed_primary"/>
                                <Column header="Gender" field="gender" />
                                <Column header="Age" field="age" />
                                <Column header="View" field="view" component={DogButton} />
                            </TableWithBrowserPagination>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default ShelterInstance;