import React, { useState, useEffect, useMemo } from 'react';
import { Table, Button, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { fetchShelters, usePaginationState } from '../../../api';
import { Paginator } from '../../paginator';
import { DataTable } from '../../datatable';
import qs from 'qs';

export const ShelterList = ({ location, history }) => {
    const [filter, setFilter] = useState({});
    const [sort, setSort] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    const [apiResponse, setApiResponse] = usePaginationState();

    useEffect(() => {
        document.title = 'Shelters';

        const query = qs.parse(location.search.slice(1));

        // extract the filter from the query string, so people can share links with filtered results
        setFilter(query.filter ?? {});
        setSort(query.sort ?? {});
        setIsLoading(true);
        fetchShelters(query.page ?? 1, query.filter ?? {}, query.sort ?? {}).then((response) => {
            setApiResponse(response);
            setIsLoading(false);
        });
    }, [location]);

    const changePage = (page) => {
        history.push({
            pathname: '/shelters',
            search: `?${qs.stringify({ page, filter, sort })}`, // on page change, lets keep the filters and sort
        });
    };

    return (
        <div>
            <div class="m-3">
                <Button variant="success" as={Link} to={'/search-shelters'}>
                    Search for shelters
                </Button>
            </div>

            <DataTable
                items={apiResponse.items}
                // pass the existing filter so that the textboxes can be populated
                filter={filter}
                // pass the existing sort so that the buttons can be highlighted
                sort={sort}
                // the property names in this columns object need to match the property names of the endpoint response, this is how it sets the filter parameters automatically
                columns={{
                    name: { display: 'name', range: true },
                    city_name: { display: 'city name', range: true },
                    state: { display: 'state', range: true },
                    available_breed_names: { display: 'available breeds' },
                    dog_count: { display: 'dog count', range: true },
                    view: { display: 'view', noFilter: true, noSort: true },
                }}
                rowMapper={(s) => (
                    <tr key={s.id}>
                        <td className="align-middle">{s.name}</td>
                        <td className="align-middle">{s.city_name}</td>
                        <td className="align-middle">{s.state}</td>
                        <td className="align-middle">{s.available_breed_names?.join(', ')}</td>
                        <td className="align-middle">{s.dog_count}</td>
                        <td className="align-middle">
                            <Button variant="success" as={Link} to={`/shelters/${s.id}`}>
                                view
                            </Button>
                        </td>
                    </tr>
                )}
                onFilterChange={(column, value) => {
                    const f = { ...filter };
                    f[column] = value;
                    setFilter(f);
                }}
                onFilterApply={() => {
                    history.push({
                        pathname: '/shelters',
                        search: `?${qs.stringify({ filter, sort })}`, // on filter application lets ignore the page (potentially zero pages)
                    });
                }}
                onSort={(column, val) => {
                    const s = {};
                    s[column] = val;
                    history.push({
                        pathname: '/shelters',
                        search: `?${qs.stringify({ filter, sort: s })}`,
                    });
                }}
            />
            <div className="d-flex justify-content-center">
                <Paginator onPageChange={changePage} pagination={apiResponse.pagination} />
            </div>
        </div>
    );
};

export default ShelterList;
