import React, { useEffect, useState } from "react";
import { fetchStats } from '../../../api';
import jackPFP from "../../../static_resources/jr_pfp.jpg";
import melPFP from "../../../static_resources/md_pfp.jpg";
import praPFP from "../../../static_resources/pk_pfp.jpg";
import sriPFP from "../../../static_resources/sa_pfp.jpg";
import patPFP from "../../../static_resources/pr_pfp.jpg";
import LoadingModal from "../../loading_modal/LoadingModal";
import gitlabIcon from "../../../static_resources/gitlab.png";
import postmanIcon from "../../../static_resources/postman.png"
import { BsChevronCompactDown } from 'react-icons/all';

const About = () => {

    const [jackStats, setJackStats] = useState({});
    const [melStats, setMelStats] = useState({});
    const [patStats, setPatStats] = useState({});
    const [praStats, setPraStats] = useState({});
    const [sriStats, setSriStats] = useState({});
    const [totalStats, setTotalStats] = useState({});
    const [isLoading, setIsLoading] = useState(true);

    // componentDidMount
    useEffect(() => {
        // Set Tab Title
        document.title = "About";

        setIsLoading(true);

        // Fetch Gitlab data
        fetchStats().then((response) => {
            if (Object.keys(response.users).length < 1) {
                return;
            }
            setJackStats(response.users['Jack Raney']);
            setMelStats(response.users['Melanie De La Cruz']);
            setPatStats(response.users['Patricio Rodriguez']);
            setPraStats(response.users['Prateeka Kodali']);
            setSriStats(response.users['Sriram Alagappan']);
            setTotalStats({ commits: response.tot_commits, issues: response.tot_issues, tests: 0 });
        });

    }, []);

    // if data loaded, turn off loading modal
    useEffect(() => {
        if (Object.keys(totalStats).length !== 0) setIsLoading(false);
    }, [totalStats]);

    return (
        <div className="about">
            <LoadingModal show={isLoading} />

            <div class="container" style={{ marginTop: '30vh', marginBottom: '20vh' }}>
                <div class="row justify-content-center text-center">
                    <div class="col-md-9">
                        <h1 class="jumbotron-heading">About DoggieDB</h1>
                        <p class="lead text-muted">DoggieDB is designed to help people
                        find a dog based on their lifestyle and location. We want people
                        to explore dogs available at shelters in their city while
                        considering what breeds might be good for them and their living
                        situation so they can adopt responsibly. The data we have gathered
                        connects specific dogs to their breed, the shelter they are located
                        at, and the city they reside in.</p>
                        <h4><a href="https://youtu.be/kLSc5EA57Ek">Presentation Video</a> </h4>
                    </div>
                </div>
            </div>

            <BsChevronCompactDown size={75} />

            <div class="container">
                <div class="row text-center">
                    <div class="col">
                        <h3 class="mb-10" style={{ marginTop: 40, marginBottom: 40 }}>About Our Team</h3>
                    </div>
                </div>
                <div class="row text-center justify-content-around">
                    <div class="col-md-4 mb-5 col-12">
                        <img alt="pfp" class="mt-3 mb-4 shadow"
                            src={jackPFP} style={{ width: 200, height: 200, borderRadius: 100, overflow: 'hidden' }} />
                        <h3><strong>Jack Raney</strong></h3>
                        {/* <p><a href="https://gitlab.com/RaneyJ">@Raneyj</a> | EID: jrr487</p> */}
                        <p>Phase II Project Leader | Backend Developer</p>
                        <p class="text-muted"> I'm a fourth year CS major at UT Austin from Beaumont,
                        Texas. My favorite type of dog is definitely Border Collies!</p>
                        <hr class="border" />
                        <ul style={{ textAlign: 'left' }}>
                            <li>Number of Commits: {(jackStats.commits) ? jackStats.commits : '0'}</li>
                            <li>Number of Issues: {(jackStats.issues) ? jackStats.issues : '0'}</li>
                            <li>Number of Unit Tests: 10</li>
                        </ul>
                    </div>
                    <div class="col-md-4 mb-5 col-12">
                        <img alt="pfp" class="mt-3 mb-4 shadow"
                            src={melPFP} style={{ width: 200, height: 200, borderRadius: 100, overflow: 'hidden' }} />
                        <h3><strong>Melanie De La Cruz</strong></h3>
                        {/* <p><a href="https://gitlab.com/melaniedlc">@melaniedlc</a> | EID: med3358</p> */}
                        <p>Phase I Project Leader | Frontend Developer</p>
                        <p class="text-muted">I'm a third year CS major at UT Austin from
                        the Dallas / Fort Worth area. My favorite dog breed is all of them
                        because all dogs are great!</p>
                        <hr class="border" />
                        <ul style={{ textAlign: 'left' }}>
                            <li>Number of Commits: {(melStats.commits) ? melStats.commits : '0'}</li>
                            <li>Number of Issues: {(melStats.issues) ? melStats.issues : '0'}</li>
                            <li>Number of Unit Tests: 20</li>
                        </ul>
                    </div>
                    <div class="col-md-4 mb-5 col-12">
                        <img alt="pfp" class="mt-3 mb-4 shadow"
                            src={patPFP} style={{ width: 200, height: 200, borderRadius: 100, overflow: 'hidden' }} />
                        <h3><strong>Patricio Rodriguez</strong></h3>
                        {/* <p><a href="https://gitlab.com/patricior">@patricior</a> | EID: pir229</p> */}
                        <p>Phase III Project Leader | Full-Stack Developer</p>
                        <p class="text-muted">I am a fourth year CS major at UT Austin from Houston, TX.
                        I graduate in May and my favorite dog breed is Labrador Retriever!</p>
                        <hr class="border" />
                        <ul style={{ textAlign: 'left' }}>
                            <li>Number of Commits: {(patStats.commits) ? patStats.commits : '0'}</li>
                            <li>Number of Issues: {(patStats.issues) ? patStats.issues : '0'}</li>
                            <li>Number of Unit Tests: {(patStats['unit tests']) ? patStats['unit tests'] : 7}</li>
                        </ul>
                    </div>
                </div>
                <div class="row text-center justify-content-around">
                    <div class="col-md-4 mb-5 col-12">
                        <img alt="pfp" class="mt-3 mb-4 shadow"
                            src={praPFP} style={{ width: 200, height: 200, borderRadius: 100, overflow: 'hidden' }} />
                        <h3><strong>Prateeka Kodali</strong></h3>
                        {/* <p><a href="https://gitlab.com/prateekakodali">@prateekakodali</a> | EID: pk8793</p> */}
                        <p>Phase IV Project Leader | Frontend Developer</p>
                        <p class="text-muted">I am a third year CS major at UT Austin from Houston, Texas.
                        My favorite dog breed is Golden Retriever!</p>
                        <hr class="border" />
                        <ul style={{ textAlign: 'left' }}>
                            <li>Number of Commits: {(praStats.commits) ? praStats.commits : '0'}</li>
                            <li>Number of Issues: {(praStats.issues) ? praStats.issues : '0'}</li>
                            <li>Number of Unit Tests: 12</li>
                        </ul>
                    </div>
                    <div class="col-md-4 mb-5 col-12">
                        <img alt="pfp" class="mt-3 mb-4 shadow"
                            src={sriPFP} style={{ width: 200, height: 200, borderRadius: 100, overflow: 'hidden' }} />
                        <h3><strong>Sriram Alagappan</strong></h3>
                        {/* <p><a href="https://gitlab.com/sriramalagappan">@sriramalagappan</a> | EID: sva359</p> */}
                        <p>AWS Manager | Frontend Developer</p>
                        <p class="text-muted">I am a second year CS major at UT Austin from San Antonio, Texas.
                        My favorite dog breed is Beagle!</p>
                        <hr class="border" />
                        <ul style={{ textAlign: 'left' }}>
                            <li>Number of Commits: {(sriStats.commits) ? sriStats.commits : '0'}</li>
                            <li>Number of Issues: {(sriStats.issues) ? sriStats.issues : '0'}</li>
                            <li>Number of Unit Tests: 7</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="jumbotron">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="display-3 text-left mb-4">Gitlab Information</h1>
                            <p class="text-left">Number of Total Commits: {(totalStats.commits) ? totalStats.commits : '0'}</p>
                            <p class="text-left">Number of Total Issues: {(totalStats.issues) ? totalStats.issues : '0'}</p>
                            <p class="text-left">Number of Unit Tests: {(totalStats.tests) ? totalStats.tests : '0'}</p>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Repository Link</h4>
                                    <a href="https://gitlab.com/melaniedlc/doggiedb">
                                        <img alt="pfp"
                                            src={gitlabIcon} style={{ width: 200, height: 200 }} />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style={{ width: '100%', height: 30 }} />

            <div class="jumbotron">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="display-3 text-left mb-4">Our Data Sources</h1>
                            {/* <p class="text-left"><a href="https://www.census.gov/quickfacts/fact/table/US/PST045219">US Census</a>
                            : </p> */}
                            <p class="text-left"><a href="https://www.petfinder.com/developers/">Pet Finder</a>
                            : Data scraped through APIs which provide information on adoptable pets</p>
                            <p class="text-left"><a href="https://dog.ceo/dog-api/breeds-list">Dog API</a>
                            : Data scraped through APIs which provides information on dog breeds</p>
                            <p class="text-left"><a href="https://openweathermap.org/api">OpenWeather</a>
                            : Data scraped through APIs which provides information on cities and weather</p>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Postman API Docs</h4>
                                    <a href="https://documenter.getpostman.com/view/14754594/TzJrDf1E">
                                        <img alt="pfp"
                                            src={postmanIcon} style={{ width: 200, height: 200 }} />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-7">
                        <img src="https://cdn.pixabay.com/photo/2015/07/28/20/55/tools-864983_1280.jpg" alt="" class="img-fluid mx-auto d-block" />
                    </div>
                    <div class="col-5">
                        <h1 class="mb-2 display-4">Tools</h1>
                        <ul class="list-features lead list-l text-left">
                        <li><a href="https://reactjs.org/">React</a>
                             : Javascript front-end framework</li>
                            <li><a href="https://getbootstrap.com/">Bootstrap</a>
                             : CSS front-end framework</li>
                            <li><a href="https://aws.amazon.com/s3/">AWS S3</a>
                             : Used to host our website</li>
                            <li><a href="https://aws.amazon.com/cloudfront/">AWS CloudFront</a>
                             : Used to deliver our website content</li>
                            <li><a href="https://aws.amazon.com/elasticbeanstalk/">AWS Elastic Beanstalk</a>
                             : Used to deploy both our frontend and backend</li>
                            <li><a href="https://flask.palletsprojects.com/en/1.1.x/">Flask</a>
                             : Used to create a web framework for the backend server</li>
                            <li><a href="https://www.namecheap.com/">Namecheap</a>
                             : Used to register our domain name</li>
                            <li><a href="https://www.postman.com/">Postman</a>
                             : Used to create our RESTful API</li>
                            <li><a href="https://gitlab.com/">GitLab</a>
                             : Used to track issues and for version control</li>
                             <li><a href="https://www.sqlalchemy.org/">SQLAlchemy</a>
                             : Used to link info from API</li>
                             <li><a href="https://jestjs.io/">Jest</a>
                             : Used to test our frontend components</li>
                             <li><a href="https://www.selenium.dev/">Selenium</a>
                             : Used to create tests for GUI</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div style={{ width: '100%', height: 30 }} />

        </div>
    );
}

export default About;