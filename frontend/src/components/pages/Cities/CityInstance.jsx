import React, { useState, useEffect } from 'react';
import { Stat } from '../../Stat';
import { fetchCity } from '../../../api';
import { Container, Table, Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import LoadingModal from '../../loading_modal/LoadingModal';
import { MdLocationCity, BsPeopleFill, FaTemperatureLow, FaThermometerEmpty, FaThermometerThreeQuarters } from 'react-icons/all';
import Map from '../../map/Map';
import { TableWithBrowserPagination, Column } from 'react-rainbow-components';
import { proper } from '../../../scripts/utilities';

export const CityInstance = (props) => {
    const [city, setCity] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    const [location, setLocation] = useState({});
    var MapComponent;

    useEffect(() => {
        // Default title
        document.title = 'City';

        setIsLoading(true);

        fetchCity(props.match.params.id).then((response) => setCity(response));
    }, []);

    // if data loaded, turn off loading modal
    useEffect(() => {
        if (Object.keys(city).length !== 0) {
            setIsLoading(false);
            // set title to city name
            document.title = city.name;

            setLocation({ lat: city.latitude, lng: city.longitude });
        }
    }, [city]);

    const BreedButton = (breed) => <Button variant="success" as={Link} to={`/breeds/${breed.row.id}`}>View</Button>
    const ShelterButton = (shelter) => <Button variant="success" as={Link} to={`/shelters/${shelter.row.id}`}>View</Button>
    const DogButton = (dog) => <Button variant="success" as={Link} to={`/dogs/${dog.row.id}`}>View</Button>

    return (
        <div>
            <LoadingModal show={isLoading} />
            <div className="row justify-content-between">
                <div className="col col-md-12 pr-md-5 pl-md-5 mt-5">
                    <div className="row align-items-center justify-content-between">
                        <div className="col-12 col-md-5  text-left">
                            <div className="mb-3 ml-3">
                                <h2>
                                    <b>{proper(city.name)}, {city.state}</b>
                                </h2>
                            </div>
                            <div className="mb-3 ml-3">
                                <div className="row">
                                    <div className="col">
                                        <div className="my-3 card card-body shadow p-4 ">
                                            <div className="row align-items-center d-flex text-md-center text-lg-left">
                                                <div className="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div className="icon-wrap text-primary my-3">
                                                        <BsPeopleFill size={30} />
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-9 mt-3 mt-lg-0 ">
                                                    <h6 className="font-weight-bold">Population</h6>
                                                    <p className=" mb-0">{city.pop}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {city.pop_density?.length < 1 ? undefined : <div className="col">
                                        <div className="my-3 card card-body shadow p-4 ">
                                            <div className="row align-items-center d-flex text-md-center text-lg-left">
                                                <div className="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div className="icon-wrap text-primary my-3">
                                                        <MdLocationCity size={30} />
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-9 mt-3 mt-lg-0 ">
                                                    <h6 className="font-weight-bold">Population Density (people per m^2)</h6>
                                                    <p className=" mb-0">{city.pop_density}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="my-3 card card-body shadow p-4 ">
                                            <div className="row align-items-center d-flex text-md-center text-lg-left">
                                                <div className="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div className="icon-wrap text-primary my-3">
                                                        <FaTemperatureLow size={30} />
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-9 mt-3 mt-lg-0">
                                                    <h6 className="font-weight-bold">Average temperature (°F)</h6>
                                                    <p className="mb-0">{city.avg_temp ?? '(unavailable)'}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <div className="my-3 card card-body shadow p-4">
                                            <div className="row align-items-center d-flex text-md-center text-lg-left">
                                                <div className="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div className="icon-wrap text-primary my-3">
                                                        <FaThermometerEmpty size={30} />
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-9 mt-3 mt-lg-0 ">
                                                    <h6 className="font-weight-bold">Minimum temperature (°F)</h6>
                                                    <p className=" mb-0">{city.min_temp ?? '(unavailable)'}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="my-3 card card-body shadow p-4 ">
                                            <div className="row align-items-center d-flex text-md-center text-lg-left">
                                                <div className="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div className="icon-wrap text-primary my-3">
                                                        <FaThermometerThreeQuarters size={30} />
                                                    </div>
                                                </div>
                                                <div className="col-12 col-md-9 mt-3 mt-lg-0">
                                                    <h6 className="font-weight-bold">Maximum temperature (°F)</h6>
                                                    <p className="mb-0">{city.max_temp ?? '(unavailable)'}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-md-5 mb-4 ml-md-auto">
                            <img alt="flag-image" className="shadow" src={'/state-flags/' + city.state + '.svg'} style={{ borderRadius: 10 }} />
                        </div>
                    </div>
                    <div class="mt-3">
                        <Map location={location} zoomLevel={5} />
                    </div>
                </div>
            </div>
            <Container fluid={true}>
                <Row>
                    <Col className="mx-0">
                        <h3>Shelters</h3>
                        <Card>
                            <TableWithBrowserPagination
                                className="shelters"
                                keyField="id"
                                isLoading={isLoading}
                                data={city.shelters}
                                pageSize={5}
                            >
                                <Column header="Name" field="name" />
                                <Column header="Dog Count" field="dog_count" />
                                <Column header="View" field="view" component={ShelterButton} />
                            </TableWithBrowserPagination>
                        </Card>
                    </Col>
                    <Col className="mx-0">
                        <h3>Breeds</h3>
                        <Card>
                            <TableWithBrowserPagination
                                className="breeds"
                                keyField="id"
                                isLoading={isLoading}
                                data={city.breeds}
                                pageSize={5}
                            >
                                <Column header="Name" field="name" />
                                <Column header="Weight" field="weight" />
                                <Column header="Height" field="height" />
                                <Column header="View" field="view" component={BreedButton} />
                            </TableWithBrowserPagination>
                        </Card>
                    </Col>
                    <Col className="mx-0">
                        <h3>Dogs</h3>
                        <Card>
                            <TableWithBrowserPagination
                                className="dogs"
                                keyField="id"
                                isLoading={isLoading}
                                data={city.dogs}
                                pageSize={5}
                            >
                                <Column header="Name" field="name" />
                                <Column header="Breed" field="breed_primary"/>
                                <Column header="Gender" field="gender" />
                                <Column header="View" field="view" component={DogButton} />
                            </TableWithBrowserPagination>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default CityInstance;