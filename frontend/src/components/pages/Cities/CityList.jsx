import React, { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { fetchCities, usePaginationState } from '../../../api';
import { Paginator } from '../../paginator';
import { DataTable } from '../../datatable';
import qs from 'qs';
import { proper } from '../../../scripts/utilities';


export const CityList = ({ location, history }) => {
    const [filter, setFilter] = useState({});
    const [sort, setSort] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    const [apiResponse, setApiResponse] = usePaginationState();

    useEffect(() => {
        document.title = 'Cities';

        const query = qs.parse(location.search.slice(1));

        setFilter(query.filter ?? {});
        setSort(query.sort ?? {});
        setIsLoading(true);
        fetchCities(query.page ?? 1, query.filter ?? {}, query.sort ?? {}).then((response) => {
            setApiResponse(response);
            setIsLoading(false);
        });
    }, [location]);

    const changePage = (page) => {
        history.push({
            pathname: '/cities',
            search: `?${qs.stringify({ page, filter, sort })}`,
        });
    };

    return (
        <div>
            <div class="m-3">
                <Button variant="success" as={Link} to={'/search-cities'}>Search for cities</Button>
            </div>

            <DataTable
                items={apiResponse.items}
                filter={filter}
                sort={sort}
                columns={{
                    name: { display: 'name', range: true },
                    state: { display: 'state' , range: true },
                    avg_temp: { display: 'avg temp', range: true },
                    pop: { display: 'population' , range: true},
                    pop_density: { display: 'population density', range: true },
                    // breedIds: { display: 'compatible breeds' },
                    view: { display: 'view', noFilter: true, noSort: true },
                }}
                rowMapper={(c) => (
                    <tr key={c.id}>
                        <td className="align-middle">{proper(c.name)}</td>
                        <td className="align-middle">{proper(c.state)}</td>
                        <td className="align-middle">{c.avg_temp}</td>
                        <td className="align-middle">{c.pop}</td>
                        <td className="align-middle">{c.pop_density ? <>{c.pop_density} / m^2</> : '(none on record)'} </td>
                        {/* <td className="align-middle">{c.breedIds?.length}</td> */}
                        <td className="align-middle">
                            <Button variant="success" as={Link} to={`/cities/${c.id}`}>
                                view
                            </Button>
                        </td>
                    </tr>
                )}
                onFilterChange={(column, value) => {
                    const f = { ...filter };
                    f[column] = value;
                    setFilter(f);
                }}
                onFilterApply={() => {
                    history.push({
                        pathname: '/cities',
                        search: `?${qs.stringify({ filter, sort })}`,
                    });
                }}
                onSort={(column, val) => {
                    const s = {};
                    s[column] = val;
                    history.push({
                        pathname: '/cities',
                        search: `?${qs.stringify({ filter, sort: s })}`,
                    });
                }}
            />

            <div className="d-flex justify-content-center">
                <Paginator onPageChange={changePage} pagination={apiResponse.pagination} />
            </div>
        </div>
    );
};

export default CityList;
