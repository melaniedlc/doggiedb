import algoliasearch from 'algoliasearch';
import { InstantSearch, SearchBox, Hits, HitsPerPage, Pagination, connectPagination } from 'react-instantsearch-dom';
import React from 'react';
import SearchCityCard from '../../search_cards/SearchCityCard';
import { Paginator } from '../Search/SearchPaginator';
import algolia from "../../../static_resources/algolia.jpg";

const searchClient = algoliasearch('9A5KEX5AGF', '0f5eecdde0dd887dde07c4a0d543f5f7');

const CustomPagination = connectPagination(Paginator);

export const CitySearch = ({ location, history }) => {

    const Hit = ({ hit }) => {
        return (
            <div className="hit">
                <div className="column" key={hit.id}>
                    <SearchCityCard hit={hit} />
                </div>
            </div>
        );
    }

    return (
        <div className="container">
            <InstantSearch
                searchClient={searchClient}
                indexName="DoggieDB_Cities"
            >
                <div class="row justify-content-between">
                    <div class="text-left mt-5">
                        <div class="mb-3">
                            <h2>
                                <b>Search Cities</b>
                            </h2>
                        </div>

                        <div className='mb-3'>
                            <SearchBox searchAsYouType={false} />
                        </div>

                        <div className="row">
                            <Hits hitComponent={Hit} />
                        </div>
                    </div >
                </div>

                <div class="mb-3">
                    <HitsPerPage
                        defaultRefinement={5}
                        items={[
                            { value: 5, label: 'Show 5 per page' },
                            { value: 10, label: 'Show 10 per page' },
                            { value: 20, label: 'Show 20 per page' },
                        ]}
                    />
                </div>

                <div className="d-flex justify-content-center">
                    <CustomPagination
                        defaultRefinement={1}
                    />
                </div>

                <div className="d-flex row justify-content-center mt-3">
                    <img
                        src={algolia}
                        style={{ width: 200, marginRight: 10, marginBottom: 10, backgroundColor: 'white' }}
                        alt="algolia"
                    />
                </div>
            </InstantSearch>
        </div >
    );
}