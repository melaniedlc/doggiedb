export const breeds = [
    {name: "American Bulldog", value: 1463},
    {name: "Pit Bull Terrier", value: 11162},
    {name:"American Staffordshire Terrier", value: 2817},
    {name: "Australian Shepherd", value: 881},
    {name: "Beagle", value: 1312},
    {name: "Border Collie", value: 925},
    {name: "Boxer", value: 2502},
    {name: "Catahoula Leopard Dog", value: 719},
    {name: "German Shepherd Dog", value: 33644},
    {name: "Great Pyrenees", value: 787},
    {name: "Labrador Retriever", value: 8198},
    {name: "Rottweiler", value: 726},
    {name: "Siberian Husky", value: 782},
    {name: "Staffordshire Bull Terrier", value: 711},
    {name: "Chihuahua", value: 3570},
    {name: "Dachshund", value: 752},
    {name: "Other", value: 7615}
]