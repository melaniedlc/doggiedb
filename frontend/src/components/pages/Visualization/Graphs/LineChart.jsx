import React, { useEffect, useState } from 'react';
import * as d3 from 'd3';

export const LineChart = (props) => {

    const [myRef, setMyRef] = useState(null);
    // for some reason graph is created twice so use this to prevent that
    const [createGraph, setCreateGraph] = useState(false);

    useEffect(() => {
        setMyRef(React.createRef());
    }, [])

    useEffect(() => {
        if (myRef && !createGraph) {

            //LineChart code from https://www.d3-graph-gallery.com/graph/line_basic.html

            // set the dimensions and margins of the graph
            // set the dimensions and margins of the graph
            var margin = { top: 30, right: 30, bottom: 70, left: 60 },
                width = 750,
                height = 400;

            // append the svg object to the body of the page
            var svg = d3.select(myRef.current)
                .append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform",
                    "translate(" + margin.left + "," + margin.top + ")");

            //Read the data
            d3.csv(props.data).then(data => {
                if (data.length > 0) {

                    setCreateGraph(true)

                    const parseDate = (d) => {
                        return d3.timeParse("%Y-%m-%d")(d);
                    }

                    // Add X axis --> it is a date format
                    var x = d3.scaleTime()
                        .domain(d3.extent(data, function (d) { return parseDate(d[props.x]); }))
                        .range([0, width]);
                    svg.append("g")
                        .attr("transform", "translate(0," + height + ")")
                        .call(d3.axisBottom(x));

                    // Add Y axis
                    var y = d3.scaleLinear()
                        .domain([props.yStart, props.yEnd])
                        .range([height, 0]);
                    svg.append("g")
                        .call(d3.axisLeft(y));

                    // Add the line
                    svg.append("path")
                        .datum(data)
                        .attr("fill", "none")
                        .attr("stroke", "steelblue")
                        .attr("stroke-width", 1.5)
                        .attr("d", d3.line()
                            .x(function (d) { return x(parseDate(d[props.x])) })
                            .y(function (d) { return y(d[props.y]) })
                        )
                }
            })
        }
    }, [myRef])

    return (
        <div ref={myRef}>

        </div>
    )
}