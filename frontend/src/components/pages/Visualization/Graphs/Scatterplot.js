import React, { useEffect, useState } from 'react';
import * as d3 from 'd3';

export const Scatterplot = (props) => {

    const [myRef, setMyRef] = useState(null);
    // for some reason graph is created twice so use this to prevent that
    const [createGraph, setCreateGraph] = useState(false);

    useEffect(() => {
        setMyRef(React.createRef());
    }, [])

    useEffect(() => {
        if (myRef && !createGraph) {

            // BarChart code from https://www.d3-graph-gallery.com/graph/barplot_animation_start.html

            // set the dimensions and margins of the graph
            var margin = { top: 30, right: 30, bottom: 70, left: 60 },
                width = 750,
                height = 400;

            // append the svg object to the body of the page
            var svg = d3.select(myRef.current)
                .append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform",
                    "translate(" + margin.left + "," + margin.top + ")");

            var color = d3.scaleOrdinal()
                .domain([-1, 0, 1, 3, 4])
                .range(["#000000",
                    "#FF8C94",
                    "#FFD3B5",
                    "#DCEDC2",
                    "#A8E6CE",])

            // Parse the Data
            d3.csv(props.data).then(data => {
                // an empty array is passed on component mount so ignore that
                if (data.length > 0) {

                    setCreateGraph(true) // look at line 7

                    //create a tooltip
                    var Tooltip = d3.select(myRef.current)
                        .append("div")
                        .style("opacity", 0)
                        .attr("class", "tooltip")
                        .style("background-color", "white")
                        .style("border", "solid")
                        .style("border-width", "2px")
                        .style("border-radius", "5px")
                        .style("padding", "5px")

                    // Add X axis
                    var x = d3.scaleLinear()
                        .domain([0, 0])
                        .range([0, width]);
                    svg.append("g")
                        .attr("class", "myXaxis")   // Note that here we give a class to the X axis, to be able to call it later and modify it
                        .attr("transform", "translate(0," + height + ")")
                        .call(d3.axisBottom(x))
                        .attr("opacity", "0")

                    // Add Y axis
                    var y = d3.scaleLinear()
                        .domain([0, props.yMax])
                        .range([height, 0]);


                    svg.append("g")
                        .call(d3.axisLeft(y)
                            .tickFormat(d3.format(".1"))
                        );

                    // Add dots
                    svg.append('g')
                        .selectAll("dot")
                        .data(data)
                        .enter()
                        .append("circle")
                        .attr("cx", function (d) { return x(d[props.x]); })
                        .attr("cy", function (d) { return y(d[props.y]); })
                        .attr("r", 7)
                        .style("fill", function (d) { return color(d[props.colorValue]) })
                        .on('mouseover', function (d) {
                            Tooltip.style("opacity", 1);
                        })
                        .on('mousemove', function (d, i) {
                            const [x, y] = d3.pointer(d);
                            var matrix = this.getScreenCTM()
                                .translate(+ x, + y);
                            Tooltip
                                .html('<u>' + i[props.label] + '</u>')
                                .style("left", (window.pageXOffset + matrix.e + 15) + "px")
                                .style("top", (window.pageYOffset + matrix.f - 30) + "px");
                        })
                        .on('mouseout', function (d) {
                            Tooltip.style("opacity", 0);
                        })

                    // new X axis
                    x.domain([0, props.xMax])
                    svg.select(".myXaxis")
                        .transition()
                        .duration(2000)
                        .attr("opacity", "1")
                        .call(d3.axisBottom(x));

                    svg.selectAll("circle")
                        .transition()
                        .delay(function (d, i) { return (i * 3) })
                        .duration(2000)
                        .attr("cx", function (d) { return x(d[props.x]); })
                        .attr("cy", function (d) { return y(d[props.y]); })

                }
            });
        }
    }, [myRef])

    return (
        <div>
            <div ref={myRef} />
            <span>[</span>
            <span style={{ color: '#000000' }}>N/A &emsp;</span>
            <span style={{ color: '#FF8C94' }}>Low Income &emsp;</span>
            <span style={{ color: '#FFD3B5' }}>Lower Middle Income &emsp;</span>
            <span style={{ color: '#DCEDC2' }}>Upper Middle Income &emsp;</span>
            <span style={{ color: '#A8E6CE' }}>Upper Income</span>
            <span>]</span>
        </div >
    )
}