import React, { useEffect, useState } from 'react';
import * as d3 from 'd3';
import countries from '../data/countries.geojson';
import { proper } from '../../../../scripts/utilities'

export const MapGraph = (props) => {

    const [myRef, setMyRef] = useState(null);
    // for some reason graph is created twice so use this to prevent that
    const [createGraph, setCreateGraph] = useState(false);

    useEffect(() => {
        setMyRef(React.createRef());
    }, [])

    useEffect(() => {
        if (myRef && !createGraph) {
            // BubbleChart code from https://www.d3-graph-gallery.com/graph/circularpacking_template.html

            setCreateGraph(true)

            // set the dimensions and margins of the graph
            var width = 1000
            var height = 590

            // append the svg object to the body of the page
            var svg = d3.select(myRef.current)
                .append("svg")
                .attr("width", width)
                .attr("height", height)

            // Add a scale for bubble size
            var size = d3.scaleLinear()
                .domain([1, 100])  // What's in the data
                .range([4, 50])  // Size in pixel

            // Map and projection
            var projection = d3.geoMercator()
                .center([-95.844032, 36.966428])                // GPS of location to zoom on
                .scale(1000)                                    // This is like the zoom
                .translate([width / 2, height / 2])

            //create a tooltip
            var Tooltip = d3.select(myRef.current)
                .append("div")
                .style("opacity", 0)
                .attr("class", "tooltip")
                .style("background-color", "white")
                .style("border", "solid")
                .style("border-width", "2px")
                .style("border-radius", "5px")
                .style("padding", "5px")

            // Load external data and boot
            d3.json(countries).then(data => {

                // Filter data
                data.features = data.features.filter(function (d) { return d.properties.name == "USA" })

                // Draw the map
                svg.append("g")
                    .selectAll("path")
                    .data(data.features)
                    .enter()
                    .append("path")
                    .attr("fill", "#e1effc")
                    .attr("d", d3.geoPath()
                        .projection(projection)
                    )
                    .style("stroke", "none")

                // Add circles:
                svg
                    .selectAll("myCircles")
                    .data(props.data)
                    .enter()
                    .append("circle")
                    .attr("cx", function (d) { return projection([d.lon, d.lat])[0] })
                    .attr("cy", function (d) { return projection([d.lon, d.lat])[1] })
                    .attr("r", function (d) { return size(d.shelter_count) })
                    .style("fill", function (d) { return 'CornflowerBlue' })
                    .attr("stroke", function (d) { return 'CornflowerBlue' })
                    .attr("stroke-width", 2)
                    .attr("fill-opacity", .4)
                    .on('mouseover', function (d) {
                        Tooltip.style("opacity", 1);
                    })
                    .on('mousemove', function (d, i) {
                        const [x, y] = d3.pointer(d);
                        var matrix = this.getScreenCTM()
                            .translate(+ x, + y);
                        Tooltip
                            .html('<u>' + proper(i.name) + '</u>' + "<br>" + " " + "State" + ": " + i.state + "<br>" + "Shelters: " + i.shelter_count)
                            .style("left", (window.pageXOffset + matrix.e + 15) + "px")
                            .style("top", (window.pageYOffset + matrix.f - 30) + "px");
                    })
                    .on('mouseout', function (d) {
                        Tooltip.style("opacity", 0);
                    })
            })
        }
    }, [myRef])

    return (
        <div ref={myRef}>

        </div>
    )
}