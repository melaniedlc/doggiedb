import React, { useEffect, useState } from 'react';
import * as d3 from 'd3';
import { colors } from '../colors'

export const BubbleChart = (props) => {

    const [myRef, setMyRef] = useState(null);
    // for some reason graph is created twice so use this to prevent that
    const [createGraph, setCreateGraph] = useState(false);

    useEffect(() => {
        setMyRef(React.createRef());
    }, [])

    useEffect(() => {
        if (myRef && !createGraph) {
            // BubbleChart code from https://www.d3-graph-gallery.com/graph/circularpacking_template.html

            // set the dimensions and margins of the graph
            var width = props.size
            var height = props.size

            // append the svg object to the body of the page
            var svg = d3.select(myRef.current)
                .append("svg")
                .attr("width", width)
                .attr("height", height)

            // Parse the Data
            d3.csv(props.data).then(data => {

                if (data.length > 0) {

                    setCreateGraph(true)

                    console.log(data)

                    // Size scale for countries
                    var size = d3.scaleLinear()
                        .domain([0, props.range])
                        .range([2, props.maxBubbleSize])

                    //create a tooltip
                    var Tooltip = d3.select(myRef.current)
                        .append("div")
                        .style("opacity", 0)
                        .attr("class", "tooltip")
                        .style("background-color", "white")
                        .style("border", "solid")
                        .style("border-width", "2px")
                        .style("border-radius", "5px")
                        .style("padding", "5px")
                        .style("margin-left", "25px")

                    // select a random color
                    const color = () => {
                        return colors[Math.floor(Math.random() * colors.length)];
                    }

                    // Initialize the circle: all located at the center of the svg area
                    const node = svg.append("g")
                        .selectAll("circle")
                        .data(data)
                        .enter()
                        .append("circle")
                        .attr("class", "node")
                        .attr("r", function (d) { return size(d[props.value]) })
                        .attr("cx", width / 2)
                        .attr("cy", height / 2)
                        .style("fill", function (d) { return color() })
                        .style("fill-opacity", 0.8)
                        .attr("stroke", "black")
                        .style("stroke-width", 1)
                        .on('mouseover', function (d) {
                            Tooltip.style("opacity", 1);
                        })
                        .on('mousemove', function (d, i) {
                            Tooltip
                                .html('<u>' + i[props.label] + '</u>' + "<br>" + " " + props.value.toUpperCase() + ": " + i[props.value])
                                .style("bottom", "0px")
                                .style("left", "0px")
                        })
                        .on('mouseout', function (d) {
                            Tooltip.style("opacity", 0);
                        })

                    // Features of the forces applied to the nodes:
                    var simulation = d3.forceSimulation()
                        .force("center", d3.forceCenter().x(width / 2).y(height / 2)) // Attraction to the center of the svg area
                        .force("charge", d3.forceManyBody().strength(.1)) // Nodes are attracted one each other of value is > 0
                        .force("collide", d3.forceCollide().strength(.2).radius(function (d) { return (size(d[props.value]) + 3) }).iterations(1)) // Force that avoids circle overlapping

                    // Apply these forces to the nodes and update their positions.
                    // Once the force algorithm is happy with positions ('alpha' value is low enough), simulations will stop.
                    simulation
                        .nodes(data)
                        .on("tick", function (d) {
                            node
                                .attr("cx", function (d) { return d.x; })
                                .attr("cy", function (d) { return d.y; })
                        });
                }
            });
        }
    }, [myRef])

    return (
        <div ref={myRef}>

        </div>
    )
}

export const BubbleChartLarge = (props) => {

    const [myRef, setMyRef] = useState(null);
    // for some reason graph is created twice so use this to prevent that
    const [createGraph, setCreateGraph] = useState(false);

    useEffect(() => {
        setMyRef(React.createRef());
    }, [])

    useEffect(() => {
        if (myRef && !createGraph) {
            // BubbleChart code from https://www.d3-graph-gallery.com/graph/circularpacking_template.html

            // set the dimensions and margins of the graph
            var width = props.size
            var height = props.size

            // append the svg object to the body of the page
            var svg = d3.select(myRef.current)
                .append("svg")
                .attr("width", width)
                .attr("height", height)

            // Parse the Data
            d3.csv(props.data).then(data => {

                if (data.length > 0) {

                    setCreateGraph(true)

                    console.log(data)

                    // Size scale for countries
                    var size = d3.scaleLinear()
                        .domain([0, props.range])
                        .range([2, props.maxBubbleSize])

                    //create a tooltip
                    var Tooltip = d3.select(myRef.current)
                        .append("div")
                        .style("opacity", 0)
                        .attr("class", "tooltip")
                        .style("background-color", "white")
                        .style("border", "solid")
                        .style("border-width", "2px")
                        .style("border-radius", "5px")
                        .style("padding", "5px")

                    // select a random color
                    const color = () => {
                        return colors[Math.floor(Math.random() * colors.length)];
                    }

                    // Initialize the circle: all located at the center of the svg area
                    const node = svg.append("g")
                        .selectAll("circle")
                        .data(data)
                        .enter()
                        .append("circle")
                        .attr("class", "node")
                        .attr("r", function (d) { return size(d[props.value]) })
                        .attr("cx", width / 2)
                        .attr("cy", height / 2)
                        .style("fill", function (d) { return color() })
                        .style("fill-opacity", 0.8)
                        .attr("stroke", "black")
                        .style("stroke-width", 1)
                        .on('mouseover', function (d) {
                            Tooltip.style("opacity", 1);
                        })
                        .on('mousemove', function (d, i) {
                            const [x, y] = d3.pointer(d);
                            var matrix = this.getScreenCTM()
                                .translate(+ x, + y);
                            Tooltip
                                .html('<u>' + i[props.label] + '</u>' + "<br>" + " " + props.value.toUpperCase() + ": " + i[props.value])
                                .attr("position", "absolute")
                                // .attr('transform', `translate(${x}, ${y})`);
                                .style("left", (window.pageXOffset + matrix.e + 15) + "px")
                                .style("top", (window.pageYOffset + matrix.f - 30) + "px");
                        })
                        .on('mouseout', function (d) {
                            Tooltip.style("opacity", 0);
                        })

                    // Features of the forces applied to the nodes:
                    var simulation = d3.forceSimulation()
                        .force("center", d3.forceCenter().x(width / 2).y(height / 2)) // Attraction to the center of the svg area
                        .force("charge", d3.forceManyBody().strength(.1)) // Nodes are attracted one each other of value is > 0
                        .force("collide", d3.forceCollide().strength(.2).radius(function (d) { return (size(d[props.value]) + 3) }).iterations(1)) // Force that avoids circle overlapping

                    // Apply these forces to the nodes and update their positions.
                    // Once the force algorithm is happy with positions ('alpha' value is low enough), simulations will stop.
                    simulation
                        .nodes(data)
                        .on("tick", function (d) {
                            node
                                .attr("cx", function (d) { return d.x; })
                                .attr("cy", function (d) { return d.y; })
                        });
                }
            });
        }
    }, [myRef])

    return (
        <div ref={myRef}>

        </div>
    )
}