import React, { useEffect, useState } from 'react';
import * as d3 from 'd3';

export const BarGraph = (props) => {

    const [myRef, setMyRef] = useState(null);
    // for some reason graph is created twice so use this to prevent that
    const [createGraph, setCreateGraph] = useState(false);

    useEffect(() => {
        setMyRef(React.createRef());
    }, [])

    useEffect(() => {
        if (myRef && !createGraph) {

            // BarChart code from https://www.d3-graph-gallery.com/graph/barplot_animation_start.html

            // set the dimensions and margins of the graph
            var margin = { top: 30, right: 30, bottom: 70, left: 60 },
                width = 750,
                height = 400;

            // append the svg object to the body of the page
            var svg = d3.select(myRef.current)
                .append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform",
                    "translate(" + margin.left + "," + margin.top + ")");

            // Parse the Data
            d3.csv(props.data).then(data => {
                // an empty array is passed on component mount so ignore that
                if (data.length > 0) {

                    setCreateGraph(true) // look at line 7

                    // X axis
                    var x = d3.scaleBand()
                        .range([0, width])
                        .domain(data.map(function (d) { return d[props.x]; }))
                        .padding(0.2);
                    svg.append("g")
                        .attr("transform", "translate(0," + height + ")")
                        .call(d3.axisBottom(x))
                        .selectAll("text")
                        .attr("transform", "translate(-10,0)rotate(-45)")
                        .style("text-anchor", "end");

                    // Add Y axis
                    var y = d3.scaleLinear()
                        .domain([props.minY, props.maxY])
                        .range([height, 0]);
                    svg.append("g")
                        .call(d3.axisLeft(y));

                    // Bars
                    svg.selectAll("mybar")
                        .data(data)
                        .enter()
                        .append("rect")
                        .attr("x", function (d) { return x(d[props.x]); })
                        .attr("width", x.bandwidth())
                        .attr("fill", "#DCEDC2")
                        // no bar at the beginning thus:
                        .attr("height", function (d) { return height - y(0); }) // always equal to 0
                        .attr("y", function (d) { return y(0); })

                    // Animation
                    svg.selectAll("rect")
                        .transition()
                        .duration(800)
                        .attr("y", function (d) { return y(d[props.y]); })
                        .attr("height", function (d) { return height - y(d.aqi); })
                        .delay(function (d, i) { return (i * 100) })
                }
            });
        }
    }, [myRef])

    return (
        <div ref={myRef}>

        </div>
    )
}