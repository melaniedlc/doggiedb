import React from 'react';
import { Container } from 'react-bootstrap';
import { BubbleChartLarge } from './Graphs/BubbleChart';
import { BarGraph } from './Graphs/BarGraph';
import { LineChart } from './Graphs/LineChart';
import { Scatterplot } from './Graphs/Scatterplot';
import aqiRanked from './data/rankedAQI.csv';
import aqiTime from './data/AQI-time.csv';
import rankedAQIFull from './data/rankedAQI-full.csv';
import './Visualization.css';

export const ProvData = ({ location, history }) => {

    return (
        <div>
            <Container fluid={true}>
                <h1 class="providerTitle">Provider Visualizations</h1>
                <div  align="center">
                    <h3 class="graphTitle margin-bottom: 10px"> AQI Ranked by Country</h3>
                    <BubbleChartLarge data={aqiRanked} range={500} size={600} value={'aqi'} label={'name'} maxBubbleSize={80} />
                </div>
                <div  align="center">
                    <h3 class="graphTitle"> AQI by Economy and Population</h3>
                    <h6>[X: AQI | Y: Population]</h6>
                    <Scatterplot data={rankedAQIFull} x={'aqi'} y={'population'} xMax={100} yMax={2000000} colorValue={'economic_code'} label={'name'} />
                </div>
                <div  align="center">
                    <h3 class="graphTitle"> Average AQI (Past 6 Months) </h3>
                    <h6>[X: Month | Y: AQI]</h6>
                    <BarGraph data={aqiTime} x={'date'} y={'aqi'} minY={70} maxY={100} />
                </div>

            </Container>
        </div>
    )
}