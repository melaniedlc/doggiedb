import React, { useState, useEffect } from 'react';
import { Table, Button, Accordion, Card, Row, Col, Container, Form } from 'react-bootstrap';
import { BubbleChartLarge } from './Graphs/BubbleChart';
import { MapGraph } from './Graphs/MapGraph';
import PieChartC from './Graphs/PieChart';
import { breeds } from './data/breed-pie';
import { mapData } from './data/mapData';
import shelterCount from './data/shelter-count-all.csv';
import './Visualization.css'

export const OurData = ({ location, history }) => {

    return (
        <div>
            <Container fluid={true}>
                <h1 class="providerTitle">Our Visualizations</h1>
                <div  align="center">
                    <h3 class="graphTitle padding-bottom: 10px"> Shelters Ranked by Number of Dogs</h3>
                    <BubbleChartLarge data={shelterCount} range={1100} size={650} value={'count'} label={'name'} maxBubbleSize={60} />
                </div>
                <div  align="center">
                    <h3 class="graphTitle padding-left: 50px"> Breed Count</h3>
                    <PieChartC data={breeds} />
                </div>
                <div  align="center">
                    <h3 class="graphTitle padding-bottom: 20px"> Cities Map</h3>
                    <h6>[Bubble Size: Number of Shelters]</h6>
                    <MapGraph data={mapData} />
                </div>
            </Container>
        </div>
    )
}
