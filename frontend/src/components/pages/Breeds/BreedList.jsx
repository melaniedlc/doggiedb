import React, { useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import { usePaginationState, fetchBreeds } from '../../../api';
import { Paginator } from '../../paginator';
import { DataTable } from '../../datatable';
import qs from 'qs';

export const BreedList = ({ location, history }) => {
    const [filter, setFilter] = useState({});
    const [sort, setSort] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    const [apiResponse, setApiResponse] = usePaginationState();

    useEffect(() => {
        document.title = 'List of Breeds';

        const query = qs.parse(location.search.slice(1));

        setFilter(query.filter ?? {});
        setSort(query.sort ?? {});
        setIsLoading(true);
        fetchBreeds(query.page ?? 1, query.filter ?? {}, query.sort ?? {}).then((response) => {
            setApiResponse(response);
            setIsLoading(false);
        });
    }, [location]);

    const changePage = (page) => {
        history.push({
            pathname: '/breeds',
            search: `?${qs.stringify({ page, filter, sort })}`,
        });
    };

    return (
        <div>
            <div class="m-3">
                <Button variant="success" as={Link} to={'/search-breeds'}>
                    Search for breeds
                </Button>
            </div>

            <DataTable
                items={apiResponse.items}
                filter={filter}
                sort={sort}
                columns={{
                    name: { display: 'name', range: true },
                    classification: { display: 'classification', range: true },
                    lifespan: { display: 'lifespan (years)', range: true },
                    weight: { display: 'weight (lbs)', range: true },
                    height: { display: 'height (in)', range: true },
                    view: { display: 'view', noFilter: true, noSort: true },
                }}
                rowMapper={(b) => (
                    <tr key={b.id}>
                        <td className="align-middle">{b.name}</td>
                        <td className="align-middle">{b.classification}</td>
                        <td className="align-middle">{b.lifespan}</td>
                        <td className="align-middle">{b.weight}</td>
                        <td className="align-middle">{b.height}</td>
                        <td className="align-middle">
                            <Button variant="success" as={Link} to={`/breeds/${b.id}`}>
                                view
                            </Button>
                        </td>
                    </tr>
                )}
                onFilterChange={(column, value) => {
                    const f = { ...filter };
                    f[column] = value;
                    setFilter(f);
                }}
                onFilterApply={() => {
                    history.push({
                        pathname: '/breeds',
                        search: `?${qs.stringify({ filter, sort })}`,
                    });
                }}
                onSort={(column, val) => {
                    const s = {};
                    s[column] = val;
                    history.push({
                        pathname: '/breeds',
                        search: `?${qs.stringify({ filter, sort: s })}`,
                    });
                }}
            />
            <div className="d-flex justify-content-center">
                <Paginator onPageChange={changePage} pagination={apiResponse.pagination} />
            </div>
        </div>
    );
};

export default BreedList;
