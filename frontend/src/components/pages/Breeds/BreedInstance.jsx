import React, { useState, useEffect } from 'react';
import { fetchBreed } from '../../../api';
import { Image } from 'react-bootstrap';
import LoadingModal from '../../loading_modal/LoadingModal';
import { GiHealthNormal, FaWeightHanging, BsArrowUpDown, FaDog } from 'react-icons/all';
import Map from '../../map/Map';
import * as utilities from '../../../scripts/utilities';
import { Container, Table, Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { TableWithBrowserPagination, Column } from 'react-rainbow-components';

export const BreedInstance = (props) => {
    const [breed, setBreed] = useState({});
    const [location, setLocation] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    var MapComponent;

    useEffect(() => {
        // Default title
        document.title = 'Dog';

        setIsLoading(true);

        fetchBreed(props.match.params.id).then((response) => setBreed(response));
    }, []);

    // if data loaded, turn off loading modal
    useEffect(() => {
        if (Object.keys(breed).length !== 0) {
            setIsLoading(false);
            // set title to breed name
            document.title = breed.name;

            setLocation({ lat: breed.lat, lng: breed.long });
        }
    }, [breed]);

    const CityButton = ( city ) => <Button variant="success" as={Link} to={`/cities/${city.row.id}`}>View</Button>
    const ShelterButton = ( shelter ) => <Button variant="success" as={Link} to={`/shelters/${shelter.row.id}`}>View</Button>
    const DogButton = ( dog ) => <Button variant="success" as={Link} to={`/dogs/${dog.row.id}`}>View</Button>

    return (
        <div class="pt-5 pb-5">
            <LoadingModal show={isLoading} />
            <div class="row justify-content-between">
                <div class="col col-md-12 pr-md-5 pl-md-5 mt-5">
                    <div class="row align-items-center justify-content-between">
                        <div class="col-12 col-md-5  text-left">
                            <div class="mb-3 ml-3">
                                <h2>
                                    <b>{breed.name}</b>
                                </h2>
                            </div>
                            <div class="mb-3 ml-3">
                                <div class="row">
                                    <div class="col">
                                        <div class="my-3 card card-body shadow p-4">
                                            <div class="row align-items-center d-flex text-md-center text-lg-left">
                                                <div class="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div class="icon-wrap text-primary my-3">
                                                        <GiHealthNormal size={30} />
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-9 mt-3 mt-lg-0 ">
                                                    <h6 class="font-weight-bold">Lifespan</h6>
                                                    <p class=" mb-0">{breed.lifespan}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="my-3 card card-body shadow p-4 ">
                                            <div class="row align-items-center d-flex text-md-center text-lg-left">
                                                <div class="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div class="icon-wrap text-primary my-3">
                                                        <FaDog size={30} />
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-9 mt-3 mt-lg-0">
                                                    <h6 class="font-weight-bold">Classification</h6>
                                                    <p class=" mb-0">{breed.classification ? breed.classification : 'None'}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="my-3 card card-body shadow p-4 ">
                                            <div class="row align-items-center d-flex text-md-center text-lg-left">
                                                <div class="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div class="icon-wrap text-primary my-3">
                                                        <BsArrowUpDown size={30} />
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-9 mt-3 mt-lg-0">
                                                    <h6 class="font-weight-bold">Height (in)</h6>
                                                    <p class=" mb-0">{breed.height}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="my-3 card card-body shadow p-4 ">
                                            <div class="row align-items-center d-flex text-md-center text-lg-left">
                                                <div class="col-12 col-sm-3 col-md-3 text-center px-0">
                                                    <div class="icon-wrap text-primary my-3">
                                                        <FaWeightHanging size={30} />
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-9 mt-3 mt-lg-0">
                                                    <h6 class="font-weight-bold">Weight (lbs)</h6>
                                                    <p class=" mb-0">{breed.weight}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ml-3">
                                <p class="mt-lg-0">{breed.bred_for ? 'Bred For: ' + breed.bred_for : ''} </p>
                                <p class="mt-lg-0">{breed.temperament ? 'Temperaments: ' + breed.temperament : ''}</p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-4 ml-md-auto">
                            <img alt="dog-image" class="shadow" src={breed.image} style={{ borderRadius: 10 }} />
                        </div>
                    </div>
                    <div class="mt-3">
                        <div class="mb-3">
                            <h3>Origin: {breed.origin ? utilities.capitalize(breed.origin) : 'Unknown'}</h3>
                        </div>
                        <Map location={location} zoomLevel={5} />
                    </div>
                </div>
            </div>

            <Container fluid={true}>
                <Row>
                    <Col className="mx-0">
                        <h3>Cities</h3>
                        <Card>
                            <TableWithBrowserPagination
                                className="cities"
                                keyField="id"
                                isLoading={isLoading}
                                data={breed.cities}
                                pageSize={5}
                            >
                                <Column header="Name" field="name" />
                                <Column header="Population" field="pop" />
                                <Column header="View" field="view" component={CityButton} />
                            </TableWithBrowserPagination>
                            
                        </Card>
                    </Col>
                    <Col className="mx-0">
                        <h3>Shelters</h3>
                        <Card>
                            <TableWithBrowserPagination
                                className="shelters"
                                keyField="id"
                                isLoading={isLoading}
                                data={breed.shelters}
                                pageSize={5}
                            >
                                <Column header="Name" field="name" />
                                <Column header="Dog Count" field="dog_count" />
                                <Column header="View" field="view" component={ShelterButton} />
                            </TableWithBrowserPagination>
                        </Card>
                    </Col>
                    <Col className="mx-0">
                        <h3>Dogs</h3>
                        <Card>
                            <TableWithBrowserPagination
                                className="dogs"
                                keyField="id"
                                isLoading={isLoading}
                                data={breed.dogs}
                                pageSize={5}
                            >
                                <Column header="Name" field="name" />
                                <Column header="Gender" field="gender" />
                                <Column header="Age" field="age" />
                                <Column header="View" field="view" component={DogButton} />
                            </TableWithBrowserPagination>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};
