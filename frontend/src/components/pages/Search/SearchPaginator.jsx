import React from 'react';
import { Pagination } from 'react-bootstrap';

// export const pageItemCount = (data) => {
//     var first = (data.per_page * data.page) - (data.per_page - 1);
//     var second = (data.per_page * data.page);

//     if (second > data.total_items)
//         second = data.total_items;
    
//     return (first.toString() + "-" + second.toString() + " out of " + data.total_items.toString());
// }

export const Paginator = ({ currentRefinement, nbPages, refine, createURL }) => {
    return (
        <div>
            <Pagination size="lg" className="mb-1">
                <Pagination.Item disabled={currentRefinement === 1} onClick={() => refine(1)}>
                    {1}
                </Pagination.Item>
                <Pagination.Ellipsis disabled={true} />
                <Pagination.Prev disabled={!(currentRefinement - 1)} onClick={() => refine(currentRefinement - 1)} />
                <Pagination.Item active={true}>{currentRefinement}</Pagination.Item>
                <Pagination.Next disabled={(currentRefinement + 1 > nbPages)} onClick={() => refine(currentRefinement + 1)} />
                <Pagination.Ellipsis disabled={true} />
                <Pagination.Item disabled={currentRefinement === nbPages} onClick={() => refine(nbPages)}>
                    {nbPages}
                </Pagination.Item>
            </Pagination>
            {/* <p className="mb-1">
                <small>{pageItemCount( currentRefinement, nbPages )}</small>
            </p> */}
        </div>
    );
};
