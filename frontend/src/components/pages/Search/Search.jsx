import algoliasearch from 'algoliasearch';
import { InstantSearch, SearchBox, Hits, HitsPerPage, Pagination, connectPagination } from 'react-instantsearch-dom';
import algolia from "../../../static_resources/algolia.jpg";
import { Button, Dropdown, DropdownButton } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Paginator } from './SearchPaginator';
import React, { useState } from 'react';
import qs from 'qs';
import SearchBreedCard from '../../search_cards/SearchBreedCard';
import SearchDogCard from '../../search_cards/SearchDogCard';
import SearchShelterCard from '../../search_cards/SearchShelterCard';
import SearchCityCard from '../../search_cards/SearchCityCard';

const DEBOUNCE_TIME = 400;

const searchClient = algoliasearch('9A5KEX5AGF', '0f5eecdde0dd887dde07c4a0d543f5f7');

const CustomPagination = connectPagination(Paginator);

const Search = ({ location, history }) => {

    const createURL = state => `?${qs.stringify(state)}`;

    const searchStateToUrl = (searchState) =>
        searchState ? `${location.pathname}${createURL(searchState)}` : '/search';

    const urlToSearchState = ({ search }) => qs.parse(search.slice(1));

    const [searchState, setSearchState] = useState(urlToSearchState(location));
    const [debouncedSetState, setDebouncedSetState] = useState(null);

    const onSearchStateChange = updatedSearchState => {
        clearTimeout(debouncedSetState);

        setDebouncedSetState(
            setTimeout(() => {
                history.push(
                    searchStateToUrl(updatedSearchState),
                    updatedSearchState
                );
            }, DEBOUNCE_TIME)
        );

        setSearchState(updatedSearchState);
    };

    const Hit = ({ hit }) => {
        if ('temperament' in hit) {
            return (
                <div className="hit">
                    <div className="column" key={hit.id}>
                        <SearchBreedCard hit={hit} />
                    </div>
                </div>
            );
        } else if ('breed_primary' in hit) {
            return (

                <div className="hit">
                    <div className="column" key={hit.id}>
                        <SearchDogCard hit={hit} />
                    </div>
                </div>

            );
        } else if ('address' in hit) {
            return (
                <div className="hit">
                    <div className="column" key={hit.id}>
                        <SearchShelterCard hit={hit} />
                    </div>
                </div>
            );
        } else if ('pop' in hit) {
            return (
                <div className="hit">
                    <div className="column" key={hit.id}>
                        <SearchCityCard hit={hit} />
                    </div>
                </div>
            );
        }

    }

    return (
        <div className="container">
            <div class="row justify-content-between">
                <div class="text-left mt-5">
                    <div class="mb-3">
                        <h2>
                            <b>Dog Results {(searchState && searchState.query) ? 'for ' + searchState.query : ''}</b>
                        </h2>
                    </div>

                    <InstantSearch
                        searchClient={searchClient}
                        indexName="DoggieDB_Dogs"
                        searchState={searchState}
                        onSearchStateChange={onSearchStateChange}
                        createURL={createURL}
                    >

                        <div style={{ display: "none" }}>
                            <SearchBox searchAsYouType={false} />
                        </div>

                        <div className="row">
                            <Hits hitComponent={Hit} />
                        </div>

                        <div class="mb-3" style={{ display: "none" }}>
                            <HitsPerPage
                                defaultRefinement={5}
                                items={[
                                    { value: 5, label: 'Show 5 per page' },
                                    { value: 10, label: 'Show 10 per page' },
                                    { value: 20, label: 'Show 20 per page' },
                                ]}
                            />
                        </div>

                        <div className="d-flex justify-content-center">
                            <CustomPagination
                                defaultRefinement={1}
                            />
                        </div>
                    </InstantSearch>

                    <div class="mb-3">
                        <h2>
                            <b>Shelter Results {(searchState && searchState.query) ? 'for ' + searchState.query : ''}</b>
                        </h2>
                    </div>

                    <InstantSearch
                        searchClient={searchClient}
                        indexName="DoggieDB_Shelters"
                        searchState={searchState}
                        onSearchStateChange={onSearchStateChange}
                        createURL={createURL}
                    >

                        <div style={{ display: "none" }}>
                            <SearchBox searchAsYouType={false} />
                        </div>

                        <div className="row">
                            <Hits hitComponent={Hit} />
                        </div>

                        <div class="mb-3" style={{ display: "none" }}>
                            <HitsPerPage
                                defaultRefinement={5}
                                items={[
                                    { value: 5, label: 'Show 5 per page' },
                                    { value: 10, label: 'Show 10 per page' },
                                    { value: 20, label: 'Show 20 per page' },
                                ]}
                            />
                        </div>

                        <div className="d-flex justify-content-center">
                            <CustomPagination
                                defaultRefinement={1}
                            />
                        </div>
                    </InstantSearch>

                    <div class="mb-3">
                        <h2>
                            <b>Breed Results {(searchState && searchState.query) ? 'for ' + searchState.query : ''}</b>
                        </h2>
                    </div>

                    <InstantSearch
                        searchClient={searchClient}
                        indexName="DoggieDB_Breeds"
                        searchState={searchState}
                        onSearchStateChange={onSearchStateChange}
                        createURL={createURL}
                    >

                        <div style={{ display: "none" }}>
                            <SearchBox searchAsYouType={false} />
                        </div>

                        <div className="row">
                            <Hits hitComponent={Hit} />
                        </div>

                        <div class="mb-3" style={{ display: "none" }}>
                            <HitsPerPage
                                defaultRefinement={5}
                                items={[
                                    { value: 5, label: 'Show 5 per page' },
                                    { value: 10, label: 'Show 10 per page' },
                                    { value: 20, label: 'Show 20 per page' },
                                ]}
                            />
                        </div>

                        <div className="d-flex justify-content-center">
                            <CustomPagination
                                defaultRefinement={1}
                            />
                        </div>
                    </InstantSearch>

                    <div class="mb-3">
                        <h2>
                            <b>City Results {(searchState && searchState.query) ? 'for ' + searchState.query : ''}</b>
                        </h2>
                    </div>

                    <InstantSearch
                        searchClient={searchClient}
                        indexName="DoggieDB_Cities"
                        searchState={searchState}
                        onSearchStateChange={onSearchStateChange}
                        createURL={createURL}
                    >

                        <div style={{ display: "none" }}>
                            <SearchBox searchAsYouType={false} />
                        </div>

                        <div className="row">
                            <Hits hitComponent={Hit} />
                        </div>

                        <div class="" style={{ display: "none" }}>
                            <HitsPerPage
                                defaultRefinement={5}
                                items={[
                                    { value: 5, label: 'Show 5 per page' },
                                    { value: 10, label: 'Show 10 per page' },
                                    { value: 20, label: 'Show 20 per page' },
                                ]}
                            />
                        </div>

                        <div className="d-flex justify-content-center">
                            <CustomPagination
                                defaultRefinement={1}
                            />
                        </div>
                    </InstantSearch>

                </div >
            </div>
            <div className="d-flex row justify-content-center mt-3">
                <img
                    src={algolia}
                    style={{ width: 200, marginRight: 10, marginBottom: 10, backgroundColor: 'white' }}
                    alt="algolia"
                />
            </div>
        </div >
    );
}

export default Search;