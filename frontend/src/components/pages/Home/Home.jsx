import React, { useEffect } from 'react'
import { Button } from 'react-bootstrap'
import dogs from "../../../static_resources/moredogs.jpg"
import "./Home.css"

const Home = () => {

    useEffect(() => {
        document.title = "DoggieDB"
    }, [])

  return (

    <div className="homeContainer">
      <img
        className="img-fluid background-size: cover;"
        src={dogs}
        alt=""
      />
      <div className="homeContent">
        <h1 className="welcome">DoggieDB</h1>
        <p className="description">Woof Woof! Find a dog based on your lifestyle and location!</p>
        <Button href="/About" className="welcButton btn-outline-light centered">Learn More</Button>
      </div>
    </div>
  );
}

export default Home;