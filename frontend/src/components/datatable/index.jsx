import React from 'react';
import { Table, Button, Form } from 'react-bootstrap';

export const FilterBox = ({ column, range, filterValue, onFilterChange }) => {
    if (range) {
        return (
            <React.Fragment>
                <Form.Control
                    type="text"
                    placeholder="value or lower bound"
                    value={filterValue?.[0] ?? ''}
                    onChange={(event) => {
                        const f = [...filterValue];
                        f[0] = event.currentTarget.value ? event.currentTarget.value : undefined;
                        onFilterChange(column, f);
                    }}
                />
                <br />
                <Form.Control
                    type="text"
                    placeholder="upper bound (optional)"
                    value={filterValue?.[1] ?? ''}
                    onChange={(event) => {
                        const f = [...filterValue];
                        f[1] = event.currentTarget.value ? event.currentTarget.value : undefined;
                        onFilterChange(column, f);
                    }}
                />
            </React.Fragment>
        );
    }
    return (
        <Form.Control
            type="text"
            placeholder="value"
            value={filterValue}
            onChange={(event) => onFilterChange(column, event.currentTarget.value ? event.currentTarget.value : undefined)}
        />
    );
};

const SortButtons = ({ column, sort, onSort }) => {
    const selectedStyle = { color: 'red', fontWeight: 'bolder' };

    return (
        <React.Fragment>
            <a
                style={sort === 'asc' ? selectedStyle : undefined}
                href=""
                onClick={(e) => {
                    e.preventDefault();
                    onSort(column, sort === 'asc' ? undefined : 'asc');
                }}
            >
                asc
            </a>
            |
            <a
                style={sort === 'desc' ? selectedStyle : undefined}
                href=""
                onClick={(e) => {
                    e.preventDefault();
                    onSort(column, sort === 'desc' ? undefined : 'desc');
                }}
            >
                desc
            </a>
        </React.Fragment>
    );
};

export const DataTable = ({ columns, items, filter, sort, rowMapper, onFilterChange, onFilterApply, onSort }) => {
    const columnEntries = Object.entries(columns);
    return (
        <Table bordered hover>
            <thead>
                <tr>
                    {columnEntries.map(([column, info]) => (
                        <th key={column}>
                            <span>{info.display} </span>
                            {info.noSort ? null : <SortButtons column={column} sort={sort?.[column] ?? undefined} onSort={onSort} />}
                        </th>
                    ))}
                </tr>
                <tr>
                    {columnEntries.map(([column, info]) => (
                        <th key={column}>
                            {info.noFilter ? null : (
                                <FilterBox column={column} range={info.range} filterValue={filter?.[column] ?? []} onFilterChange={onFilterChange} />
                            )}
                        </th>
                    ))}
                </tr>
                <tr>
                    <th colSpan={columnEntries.length}>
                        <Button className="d-block w-100" onClick={onFilterApply}>
                            apply filters
                        </Button>
                    </th>
                </tr>
            </thead>
            <tbody>{items.map(rowMapper)}</tbody>
        </Table>
    );
};
