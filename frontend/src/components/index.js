export { default as Home } from "./pages/Home/Home";
export { default as About } from "./pages/About/About";
export { default as Search } from "./pages/Search/Search";