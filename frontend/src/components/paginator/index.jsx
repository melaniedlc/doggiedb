import React from 'react';
import { Pagination } from 'react-bootstrap';

export const pageItemCount = (data) => {
    var first = (data.per_page * data.page) - (data.per_page - 1);
    var second = (data.per_page * data.page);

    if (second > data.total_items)
        second = data.total_items;
    
    return (first.toString() + "-" + second.toString() + " out of " + data.total_items.toString());
}

export const Paginator = ({ onPageChange, pagination }) => {
    return (
        <div>
            <Pagination size="lg" className="mb-1">
                <Pagination.Item disabled={pagination.page === 1} onClick={() => onPageChange(1)}>
                    {1}
                </Pagination.Item>
                <Pagination.Ellipsis disabled={true} />
                <Pagination.Prev disabled={!pagination.prev_page} onClick={() => onPageChange(pagination.prev_page)} />
                <Pagination.Item active={true}>{pagination.page}</Pagination.Item>
                <Pagination.Next disabled={!pagination.next_page} onClick={() => onPageChange(pagination.next_page)} />
                <Pagination.Ellipsis disabled={true} />
                <Pagination.Item disabled={pagination.page === pagination.total_pages} onClick={() => onPageChange(pagination.total_pages)}>
                    {pagination.total_pages}
                </Pagination.Item>
            </Pagination>
            <p className="mb-1">
                <small>{pageItemCount(pagination)}</small>
            </p>
        </div>
    );
};
