import React from 'react';
import GoogleMapReact from 'google-map-react';
import './map.css';
import { GrLocationPin } from 'react-icons/gr';

const Map = ({ location, zoomLevel }) => {
    // only render if location is given
    if (Object.keys(location).length !== 0) {
        return (
            <div className="map">
                <div className="google-map">
                    <GoogleMapReact bootstrapURLKeys={{ key: 'AIzaSyAx-pQznV8GvrMmpU7habCxbsqYM2SGrJk' }} defaultCenter={location} defaultZoom={zoomLevel}>
                        <GrLocationPin size="3em" lat={location.lat} lng={location.lng} />
                    </GoogleMapReact>
                </div>
            </div>
        );
    } else {
        return <div />;
    }
};

export default Map;
