import React from 'react';

export const Stat = ({ label, value }) => (
    <p>
        {label} <span className="font-weight-bold">{value ?? '(none on record)'}</span>
    </p>
);
