import React from "react";
import { Card } from "react-bootstrap";
import { Highlight } from "react-instantsearch-dom";

function SearchBreedCard(props) {

    // let breed = props.hit;

    const breed_attributes = [
        {
            name: "Name:",
            attribute: "name",
            value: props.hit.name,
            attribute_id: 0
        },
        {
            name: "Classification:",
            attribute: "classification",
            value: props.hit.classification,
            attribute_id: 1
        },
        {
            name: "Lifespan:",
            attribute: "lifespan",
            value: props.hit.lifespan,
            attribute_id: 2
        }
    ]

    const displayBreedText = () => {
        return (
            breed_attributes.map((breed) => (
                <Card.Text className="content" key={breed.attribute_id}>
                    <b>{breed.name} {" "}</b>
                    <Highlight attribute={breed.attribute}
                        tagName="mark"
                        nonHighlightedTagName="span"
                        hit={props.hit} />
                    {/* {!(breed.value.toString().includes(props.hit.name.toString())) ? breed.value : <b></b>} */}
                </Card.Text>
            ))
        );
    }

    return (
        <Card style={{ width: '75rem', marginBottom: 10 }}>
            <Card.Body>
                <div className='row'>
                    <Card.Img variant="top" src={props.hit.image ?? props.hit.image} style={{ width: 300 }} />
                    <div className='col'>
                        <a href={"/breeds/" + props.hit.id}>
                            <u>
                                <Card.Title>
                                    {props.hit.name}
                                </Card.Title>
                            </u>
                        </a>
                        {displayBreedText()}
                        <br />
                    </div>
                </div>
            </Card.Body>
        </Card>
    );
}

export default SearchBreedCard;