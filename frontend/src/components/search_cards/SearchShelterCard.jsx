import React from "react";
import { Card } from "react-bootstrap";
import { Highlight } from "react-instantsearch-dom";

function SearchShelterCard(props) {

    const shelter_attributes = [
        {
            name: "Name:",
            attribute: "name",
            value: props.hit.name,
            attribute_id: 0
        },
        {
            name: "City:",
            attribute: "city_name",
            value: props.hit.city_name,
            attribute_id: 1
        },
        {
            name: "State:",
            attribute: "state",
            value: props.hit.state,
            attribute_id: 2
        }
    ]

    const displayShelterText = () => {
        return (
            shelter_attributes.map((shelter) => (
                <Card.Text className="content" key={shelter.attribute_id}>
                    <b>{shelter.name} {" "}</b>
                    <Highlight attribute={shelter.attribute}
                        tagName="mark"
                        nonHighlightedTagName="span"
                        hit={props.hit} />
                    {/* {!(shelter.value.toString().includes(props.hit.name.toString())) ? shelter.value : <b></b>} */}
                </Card.Text>
            ))
        );
    }

    return (
        <Card style={{ width: '75rem', marginBottom: 10 }}>
            <Card.Body>
                <a href={"/shelters/" + props.hit.id}>
                    <u>
                        <Card.Title>
                            {props.hit.name}
                        </Card.Title>
                    </u>
                </a>
                {displayShelterText()}
                <br />
            </Card.Body>
        </Card>
    );
}

export default SearchShelterCard;