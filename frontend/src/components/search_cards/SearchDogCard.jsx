import React from "react";
import { Card } from "react-bootstrap";
import { Highlight } from "react-instantsearch-dom";

function SearchDogCard(props) {

    const dog_attributes = [
        {
            name: "Name:",
            attribute: "name",
            value: props.hit.name,
            attribute_id: 0
        },
        {
            name: "Breed:",
            attribute: "breed_primary",
            value: props.hit.breed_primary,
            attribute_id: 1
        },
        {
            name: "Age:",
            attribute: "age",
            value: props.hit.age,
            attribute_id: 2
        }
    ]

    const displayDogText = () => {
        return (
            dog_attributes.map((dog) => (
                <Card.Text className="content" key={dog.attribute_id}>
                    <b>{dog.name} {" "}</b>
                    <Highlight attribute={dog.attribute}
                        tagName="mark"
                        nonHighlightedTagName="span"
                        hit={props.hit} />
                    {/* {!(dog.value.toString().includes(props.hit.name.toString())) ? dog.value : <b></b>} */}
                </Card.Text>
            ))
        );
    }

    return (
        <Card style={{ width: '75rem', marginBottom: 10 }}>
            <Card.Body>
                <div className='row'>
                    <Card.Img variant="top" src={props.hit.photo ?? props.hit.photo} style={{ width: 300 }} />
                    <div className='col'>
                        <a href={"/dogs/" + props.hit.id}>
                            <u>
                                <Card.Title>
                                    {props.hit.name}
                                </Card.Title>
                            </u>
                        </a>
                        {displayDogText()}
                        <br />
                    </div>
                </div>
            </Card.Body>
        </Card>
    );
}

export default SearchDogCard;