import React from "react";
import { Card } from "react-bootstrap";
import { Highlight } from "react-instantsearch-dom";
import { proper } from '../../scripts/utilities';

function SearchCityCard(props) {

    const city_attributes = [
        {
            name: "Name:",
            attribute: "name",
            value: props.hit.name,
            attribute_id: 0
        },
        {
            name: "State:",
            attribute: "state",
            value: props.hit.state,
            attribute_id: 1
        },
        {
            name: "Population:",
            attribute: "pop",
            value: props.hit.pop,
            attribute_id: 2
        }
    ]

    const displayCityText = () => {
        return (
            city_attributes.map((city) => (
                <Card.Text className="content" key={city.attribute_id}>
                    <b>{city.name} {" "}</b>
                    <Highlight attribute={city.attribute}
                        tagName="mark"
                        nonHighlightedTagName="span"
                        hit={props.hit} /> 
                    {/* {!(city.value.toString().includes(props.hit.name.toString())) ? city.value : <b></b>} */}
                </Card.Text>
            ))
        );
    }

    return (
        <Card style={{ width: '75rem', marginBottom: 10 }}>
            <Card.Body>
                <a href={"/cities/" + props.hit.id}>
                    <u>
                        <Card.Title>
                            {proper(props.hit.name)}
                        </Card.Title>
                    </u>
                </a>
                {displayCityText()}
                <br />
            </Card.Body>
        </Card>
    );
}

export default SearchCityCard;