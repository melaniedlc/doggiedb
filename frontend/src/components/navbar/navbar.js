import React, { Component } from 'react';
import styles from './navbar.css';
import Navbar from 'react-bootstrap/Navbar';
import { AiOutlineSearch } from "react-icons/ai";
import { Nav, Form, FormControl, Button, NavDropdown, InputGroup } from 'react-bootstrap';

const CustomNavbar = () => {
    const textInput = React.useRef();

    function searchOnClick() {
        window.location.assign("/search?query=" + textInput.current.value + "&page=1");
    }

    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand className="emoji" href="/">🐶</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="/breeds">Breeds</Nav.Link>
                    <Nav.Link href="/shelters">Shelters</Nav.Link>
                    <Nav.Link href="/cities">Cities</Nav.Link>
                    <Nav.Link href="/dogs">Dogs</Nav.Link>
                    <Nav.Link href="/nearby">Nearby</Nav.Link>
                    <Nav.Link href="/ourdata">OurData</Nav.Link>
                    <Nav.Link href="/provdata">ProviderData</Nav.Link>
                    <Nav.Link href="/about">About</Nav.Link>
                </Nav>
                <Form inline onSubmit={(e) => {e.preventDefault();}}>
                    <InputGroup>
                        {/* search bar */}
                        <FormControl
                            className="mr-sm-2"
                            type="text"
                            placeholder="Search"
                            ref={textInput}
                            onKeyPress={(event) => {
                                if (event.key === "Enter") {
                                searchOnClick();
                                }
                            }}
                        />
                        {/* search button */}
                        <InputGroup.Append style={{fontSize: "1.1vw"}}>
                        <Button
                            variant="outline-primary"
                            onClick={() => searchOnClick()}
                        >
                        <AiOutlineSearch />
                        </Button>
                    </InputGroup.Append>
                    </InputGroup>
                </Form>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default CustomNavbar