import React from 'react'
import './LoadingModal.css';
import ReactLoading from 'react-loading'
import Lottie from 'react-lottie';
import animationData from '../../static_resources/19979-dog-steps.json';

const LoadingModal = ({ show }) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";

  const defaultOptions = {
    loop: true,
    autoplay: true, 
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  return (
    <div className={showHideClassName}>
      <section className="modal-main">
          <Lottie options={defaultOptions}
              height={300}
              width={300}/>
      </section>
    </div>
  );
};

export default LoadingModal