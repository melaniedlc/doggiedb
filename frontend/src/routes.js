import React from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';

// Pages
import { Home, About, Search } from './components'
import { ShelterList } from './components/pages/Shelters/ShelterList';
import { ShelterInstance } from './components/pages/Shelters/ShelterInstance';
import { CityList } from './components/pages/Cities/CityList';
import { CityInstance } from './components/pages/Cities/CityInstance';
import { DogList } from './components/pages/Dogs/DogList';
import { DogInstance } from './components/pages/Dogs/DogInstance';
import { BreedList } from './components/pages/Breeds/BreedList';
import { BreedInstance } from './components/pages/Breeds/BreedInstance';
import { ProximityPage } from './components/pages/Proximity/Proximity';
import { BreedSearch } from './components/pages/Breeds/BreedSearch';
import { ShelterSearch } from './components/pages/Shelters/ShelterSearch';
import { CitySearch } from './components/pages/Cities/CitySearch';
import { DogSearch } from './components/pages/Dogs/DogSearch';
import { OurData } from './components/pages/Visualization/Visualization'
import { ProvData } from './components/pages/Visualization/ProvVisualization'

const Routes = () => (
    <BrowserRouter >
        <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/about" exact component={About} />
            <Route path="/breeds" exact component={BreedList} />
            <Route path="/breeds/:id" exact component={BreedInstance} />
            <Route path="/shelters" exact component={ShelterList} />
            <Route path="/shelters/:id" exact component={ShelterInstance} />
            <Route path="/cities" exact component={CityList} />
            <Route path="/cities/:id" exact component={CityInstance} />
            <Route path="/dogs" exact component={DogList} />
            <Route path="/dogs/:id" exact component={DogInstance} />
            <Route path="/search" exact component={Search} />
            <Route path="/nearby" exact component={ProximityPage} />
            <Route path="/search-breeds" exact component={BreedSearch} />
            <Route path="/search-shelters" exact component={ShelterSearch} />
            <Route path="/search-cities" exact component={CitySearch} />
            <Route path="/search-dogs" exact component={DogSearch} />
            <Route path="/ourdata" exact component={OurData}/>
            <Route path="/provdata" exact component={ProvData}/>

        </Switch>
    </BrowserRouter>
);
export default Routes;