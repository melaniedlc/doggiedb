export function capitalize(word) {
    const result = word[0].toUpperCase() + word.substr(1).toLowerCase();
    return result;
}

export function proper(sentence) {
    if (!sentence) return '';
    const words = sentence.split(" ");

    for (let i = 0; i < words.length; i++) {
        words[i] = words[i][0].toUpperCase() + words[i].substr(1);
    }

    return words.join(" ");;
}