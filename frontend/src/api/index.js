import { useState } from 'react';
import qs from 'qs';


// const apiUrl = 'http://127.0.0.1:5000/api/';
const apiUrl = 'https://www.doggiedb.me/api/';

const defaultPaginationResponse = () => ({
    items: [],
    pagination: { next_page: null, prev_page: null, page: 1, per_page: 20, total_items: 0, total_pages: 1 },
});

export const usePaginationState = () => useState(defaultPaginationResponse());

export const fetchApi = async (endpoint, defaultReturn, queryParams = {}) => {
    try {
        const response = await fetch(apiUrl + endpoint + `?${qs.stringify(queryParams, { indices: false })}`);
        return await response.json();
    } catch (e) {
        console.log(e);
        return defaultReturn;
    }
};

export const fetchStats = () => fetchApi('get_stats', {});

export const fetchProximitySearch = async (queryParams) => {
    const json = await fetchApi('nearby', defaultPaginationResponse(), queryParams);
    return { ...defaultPaginationResponse(), items: json.items.map(mapCityToJs) };
}

export const mapBreedToJs = (value) => ({
    ...value,

    dogs: value?.['dogs']?.map(mapDogToJs),
    cities: value?.['cities']?.map(mapCityToJs),
    shelters: value?.['shelters']?.map(mapShelterToJs),
});
export const fetchBreeds = async (page = 1, filter = {}, sort = {}) => {
    const json = await fetchApi('breeds', defaultPaginationResponse(), { page, filter, sort });
    return { pagination: json.pagination, items: json.items.map(mapBreedToJs) };
};
export const fetchBreed = async (id) => {
    const b = await fetchApi(`breeds/${id}`, {});
    return mapBreedToJs(b);
};

export const mapShelterToJs = (value) => ({
    ...value,

    dogs: value?.['dogs']?.map(mapDogToJs),
    breeds: value?.['breeds']?.map(mapBreedToJs),
    city: value?.['city'] !== undefined ? mapCityToJs(value['city']) : undefined,
});
export const fetchShelters = async (page = 1, filter = {}, sort = {}) => {
    const json = await fetchApi('shelters', defaultPaginationResponse(), { page, filter, sort });
    return { pagination: json.pagination, items: json.items.map(mapShelterToJs) };
};
export const fetchShelter = async (id) => {
    const b = await fetchApi(`shelters/${id}`, {});
    return mapShelterToJs(b);
};

export const mapCityToJs = (value) => ({
    ...value,

    dogs: value?.['dogs']?.map(mapDogToJs),
    breeds: value?.['breeds']?.map(mapBreedToJs),
    shelters: value?.['shelters']?.map(mapShelterToJs),
});
export const fetchCities = async (page = 1, filter = {}, sort = {}) => {
    const json = await fetchApi('cities', defaultPaginationResponse(), { page, filter, sort });
    return { pagination: json.pagination, items: json.items.map(mapCityToJs) };
};
export const fetchCity = async (id) => {
    const b = await fetchApi(`cities/${id}`, {});
    return mapCityToJs(b);
};

export const mapDogToJs = (value) => ({
    ...value,

    city: value?.['city'] !== undefined ? mapCityToJs(value['city']) : undefined,
    breed: value?.['breed'] !== undefined ? mapBreedToJs(value['breed']) : undefined,
    shelter: value?.['shelter'] !== undefined ? mapShelterToJs(value['shelter']) : undefined,
});
export const fetchDogs = async (page = 1, filter = {}, sort = {}) => {
    const json = await fetchApi('dogs', defaultPaginationResponse(), { page, filter, sort });
    return { pagination: json.pagination, items: json.items.map(mapDogToJs) };
};
export const fetchDog = async (id) => {
    const b = await fetchApi(`dogs/${id}`, {});
    return mapDogToJs(b);
};
