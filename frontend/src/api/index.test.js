import { fetchApi, mapBreedToJs, mapCityToJs, mapDogToJs, mapShelterToJs } from '.';

const testModel = { values: 'here', dont: 'matter', test: 100 };

test('fetch default return on error', async () => {
    const result = await fetchApi('does not exist', { defaultReturnTest: true }, {});
    expect(result).toEqual({ defaultReturnTest: true });
});

test('map breed', () => {
    expect(mapBreedToJs(testModel)).toEqual({ ...testModel, dogs: undefined, cities: undefined, shelters: undefined });
});

test('map shelter', () => {
    expect(mapShelterToJs(testModel)).toEqual({ ...testModel, dogs: undefined, breeds: undefined, city: undefined });
});

test('map city', () => {
    expect(mapCityToJs(testModel)).toEqual({ ...testModel, dogs: undefined, breeds: undefined, shelters: undefined });
});

test('map dog', () => {
    expect(mapDogToJs(testModel)).toEqual({ ...testModel, city: undefined, breed: undefined, shelter: undefined });
});
