import React, { Component } from 'react';
import './App.css';
import Routes from './routes';
import Navbar from './components/navbar/navbar'
import Footbar from './components/footbar/footbar'

class App extends Component {

  render() {
    // default title
    document.title="DoggieDB";
    return (
      <div className="App">
        <Navbar />
        <Routes />
      </div>
    );
  }
}

export default App;
