import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16"
import Cities from "../components/pages/Cities/CityList"

configure({ adapter: new Adapter() })

describe("Test Cities Model Page", () => {
	test("Cities", () => {
		const citiesTest = shallow(<Cities />)
		expect(citiesTest).toMatchSnapshot()
	})
})
