import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16"
import Breeds from "../components/pages/Breeds/BreedList"

configure({ adapter: new Adapter() })

describe("Test Breeds Model Page", () => {
	test("Breeds", () => {
		const breedsTest = shallow(<Breeds />)
		expect(breedsTest).toMatchSnapshot()
	})
})
