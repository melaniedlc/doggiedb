import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16"
import Dogs from "../components/pages/Dogs/DogList"

configure({ adapter: new Adapter() })

describe("Test Dogs Model Page", () => {
	test("Dogs", () => {
		const dogsTest = shallow(<Dogs />)
		expect(dogsTest).toMatchSnapshot()
	})
})
