import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16"
import Shelters from "../components/pages/Shelters/ShelterList"

configure({ adapter: new Adapter() })

describe("Test Shelters Model Page", () => {
	test("Shelters", () => {
		const sheltersTest = shallow(<Shelters />)
		expect(sheltersTest).toMatchSnapshot()
	})
})
