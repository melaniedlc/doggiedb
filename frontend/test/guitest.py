import unittest
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = "https://www.doggiedb.me"
CHROMEDRIVER_PATH = '/usr/bin/chromedriver'
WINDOW_SIZE = "1920,1080"

class GUITests(unittest.TestCase):
    def setUp(self):
        print('\nSetting up GUI Tests with Selenium')
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--window-size=%s" % WINDOW_SIZE)
        chrome_options.add_argument('--no-sandbox')
        self.driver = webdriver.Chrome(executable_path=CHROMEDRIVER_PATH,
            options=chrome_options
        )
        print('Browser successfully set up')
        self.driver.implicitly_wait(20)
        self.driver.get(URL)
        print('Navigated to URL')
        
    def test_homepage(self):
        print('Testing that site opens to home page')
        result = self.driver.title
        actual = 'DoggieDB'
        assert(result == actual)
        print('SUCCESS')

    def test_tabs(self):
        print('Testing Tabs')
        breedLink = self.driver.find_element_by_link_text('Breeds')
        sheltersLink = self.driver.find_element_by_link_text('Shelters')
        citiesLink = self.driver.find_element_by_link_text('Cities')
        dogsLink = self.driver.find_element_by_link_text('Dogs')
        aboutLink = self.driver.find_element_by_link_text('About')
        print('SUCCESS')
        
    def test_pagination(self):
        print('Testing Pagination')
        self.driver.find_element_by_link_text('Breeds').click()
        self.driver.implicitly_wait(20)
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/breeds"
        assert(result == actual)
        
        self.driver.find_element_by_xpath("//div[@id='root']/div/div/div[3]/div/ul/li[5]/a/span").click()
        self.driver.implicitly_wait(20)
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/breeds?page=2"
        assert(result == actual)
        
        self.driver.find_element_by_xpath("//div[@id='root']/div/div/div[3]/div/ul/li[3]/a").click()
        self.driver.implicitly_wait(20)
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/breeds?page=1"
        assert(result == actual)
        
        self.driver.find_element_by_link_text('9').click()
        self.driver.implicitly_wait(20)
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/breeds?page=9"
        assert(result == actual)
        
        self.driver.find_element_by_link_text('1').click()
        self.driver.implicitly_wait(20)
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/breeds?page=1"
        assert(result == actual)
        
        print('SUCCESS')
        
    def test_breed(self):
        print('Testing Breed Instance Page')
        self.driver.find_element_by_link_text('Breeds').click()
        self.driver.implicitly_wait(20)
        
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/breeds"
        assert(result == actual)
        
        print('SUCCESS')
        
    def test_shelter(self):
        print('Testing Shelter Instance Page')
        self.driver.find_element_by_link_text('Shelters').click()
        self.driver.implicitly_wait(20)
        
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/shelters"
        assert(result == actual)
        
        self.driver.find_element_by_xpath("//a[contains(@href, '/shelters/650')]").click()
        self.driver.implicitly_wait(20)
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/shelters/650"
        assert(result == actual)
        
        print('SUCCESS')
        
    def test_city(self):
        print('Testing Cities Instance Page')
        self.driver.find_element_by_link_text('Cities').click()
        self.driver.implicitly_wait(20)
        
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/cities"
        assert(result == actual)
        
        self.driver.find_element_by_xpath("//a[contains(@href, '/cities/1')]").click()
        self.driver.implicitly_wait(20)
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/cities/1"
        assert(result == actual)
        
        print('SUCCESS')
        
    def test_dogs(self):
        print('Testing Dogs Instance Page')
        self.driver.find_element_by_link_text('Dogs').click()
        self.driver.implicitly_wait(20)
        
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/dogs"
        assert(result == actual)
        
        self.driver.find_element_by_xpath("//a[contains(@href, '/dogs/1')]").click()
        self.driver.implicitly_wait(20)
        result = self.driver.current_url
        actual = "https://www.doggiedb.me/dogs/1"
        assert(result == actual)
        
        print('SUCCESS')

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
